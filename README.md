 
## Overview
 
This repository contains information and code regarding the work developed for the Master Thesis:

*  M. de Sousa Ribeiro, “[Neural and Symbolic AI - mind the gap! Aligning Artificial Neural Networks and Ontologies](http://hdl.handle.net/10362/113651),” Master’s thesis, NOVA School of Science and Technology, NOVA University Lisbon, 2021.
 

## Neural Networks' Architectures

The neural network architectures used throughout the experiments performed in the context of this dissertation can be found in folders `nns/conv` and `nns/dense`.
The mapping networks architectures can be found in folder `nns/mapping_nns`.

## Justification Platform - Visual Demo
 
The Justification Platform Demo can be found in folder `justification_platform_demo`.
 
## Explainable Abstract Trains Dataset

More details regarding the Explainable Abstract Trains Dataset used in this dissertation can be found in:

* https://bitbucket.org/xtrains/dataset/

