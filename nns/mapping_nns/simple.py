class Simple:
    @staticmethod
    def build(num_inputs, classes):
        from tensorflow.keras import Sequential
        from tensorflow.keras.layers import Activation, Dense

        # initialize the model
        model = Sequential()

        model.add(Dense(10, input_dim=num_inputs))
        model.add(Activation('relu'))

        model.add(Dense(5, input_dim=num_inputs))
        model.add(Activation('relu'))

        # softmax classifier
        model.add(Dense(classes))
        if classes > 1:
            model.add(Activation('softmax'))
        else:
            model.add(Activation('sigmoid'))

        # return the constructed network architecture
        return model
