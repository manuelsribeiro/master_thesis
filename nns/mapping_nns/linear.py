class Linear:
    @staticmethod
    def build(num_inputs, classes):
        from tensorflow.keras import Sequential
        from tensorflow.keras.layers import Activation, Dense

        # initialize the model
        model = Sequential()

        # softmax classifier
        model.add(Dense(classes, input_dim=num_inputs))
        if classes > 1:
            model.add(Activation('softmax'))
        else:
            model.add(Activation('sigmoid'))

        # return the constructed network architecture
        return model
