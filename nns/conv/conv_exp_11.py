class ConvExp11:
    @staticmethod
    def build(width, height, depth, classes):
        from tensorflow.keras import Sequential
        from tensorflow.keras.layers import Activation, BatchNormalization, Conv2D, Dense, Dropout, Flatten, LeakyReLU, \
            MaxPool2D
        from tensorflow.keras import backend as K

        # initialize the model
        model = Sequential()
        input_shape = (height, width, depth)
        chan_dim = -1

        # if using 'channels first', update the input shape and channels dimension
        if K.image_data_format() == 'channels_first':
            input_shape = (depth, height, width)
            chan_dim = 1

        # first CONV => RELU => CONV => RELU => POOL => DO layer set
        model.add(Conv2D(32, (3, 3), padding='same', input_shape=input_shape))
        model.add(Activation('relu'))
        model.add(BatchNormalization(axis=chan_dim))
        model.add(Conv2D(32, (3, 3), padding='same'))
        model.add(Activation('relu'))
        model.add(BatchNormalization(axis=chan_dim))
        model.add(MaxPool2D(pool_size=(2, 2)))
        model.add(Dropout(rate=0.2))

        # first CONV => RELU => POOL => CONV => RELU => POOL => DO layer set
        model.add(Conv2D(64, (3, 3), padding='same'))
        model.add(Activation('relu'))
        model.add(BatchNormalization(axis=chan_dim))
        model.add(MaxPool2D(pool_size=(2, 2)))
        model.add(Conv2D(64, (3, 3), padding='same'))
        model.add(Activation('relu'))
        model.add(BatchNormalization(axis=chan_dim))
        model.add(MaxPool2D(pool_size=(2, 2)))
        model.add(Dropout(rate=0.2))

        # second CONV => RELU => POOL => CONV => RELU => POOL => DO layer set
        model.add(Conv2D(128, (3, 3), padding='same'))
        model.add(Activation('relu'))
        model.add(BatchNormalization(axis=chan_dim))
        model.add(MaxPool2D(pool_size=(2, 2)))
        model.add(Conv2D(128, (3, 3), padding='same'))
        model.add(Activation('relu'))
        model.add(BatchNormalization(axis=chan_dim))
        model.add(MaxPool2D(pool_size=(2, 2)))
        model.add(Dropout(rate=0.2))

        # first (and only) set of FC => lRELU layers
        model.add(Flatten())
        model.add(Dense(16))
        model.add(LeakyReLU(alpha=0.05))
        model.add(BatchNormalization())

        # softmax classifier
        model.add(Dense(classes))
        if classes > 1:
            model.add(Activation('softmax'))
        else:
            model.add(Activation('sigmoid'))

        # return the constructed network architecture
        return model