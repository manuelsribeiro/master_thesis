class MobileNetV2Exp:
    @staticmethod
    def build(width, height, depth, classes):
        from tensorflow.keras import Input, Model
        from tensorflow.keras.applications import MobileNetV2
        from tensorflow.keras.layers import Activation, Dropout, Dense, Flatten
        from tensorflow.keras import backend as K

        input_shape = (height, width, depth)

        # if using 'channels first', update the input shape and channels dimension
        if K.image_data_format() == 'channels_first':
            input_shape = (depth, height, width)

        # initialize the model along with the input shape to be 'channels last'
        inputs = Input(shape=input_shape)
        base_model = MobileNetV2(input_shape=input_shape, include_top=False, weights='imagenet')

        x = base_model(inputs, training=True)
        x = Dropout(rate=0.2)(x)

        x = Flatten()(x)
        x = Dense(classes)(x)
        if classes > 1:
            outputs = Activation('softmax')(x)
        else:
            outputs = Activation('sigmoid')(x)

        # return the constructed network architecture
        model = Model(inputs, outputs)
        return model
