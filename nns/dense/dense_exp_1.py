class DenseExp1:
    @staticmethod
    def build(width, height, depth, classes):
        from tensorflow.keras import Sequential
        from tensorflow.keras.layers import Activation, Dense, Flatten
        from tensorflow.keras import backend as K

        # initialize the model
        model = Sequential()
        input_shape = (height, width, depth)

        # if using 'channels first', update the input shape and channels dimension
        if K.image_data_format() == 'channels_first':
            input_shape = (depth, height, width)

        model.add(Flatten(input_shape=input_shape))
        model.add(Dense(512, input_shape=(width*height*depth,)))
        model.add(Activation('relu'))
        model.add(Dense(256))
        model.add(Activation('relu'))
        model.add(Dense(128))
        model.add(Activation('relu'))

        # softmax classifier
        model.add(Dense(classes))
        if classes > 1:
            model.add(Activation('softmax'))
        else:
            model.add(Activation('sigmoid'))

        # return the constructed network architecture
        return model
