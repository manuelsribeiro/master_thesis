from PIL import ImageTk, Image
from tkinter import filedialog as tkfiledialog
from tkinter import font as tkfont
from tkinter import messagebox as tkmessagebox

import glob
import multiprocessing
import tkinter as tk
import os
import re

APP_NAME = 'Neuro-Symbolic Justification Platform'
TRAINS_DATASET = './datasets/explainable_abstract_trains_subset/images/'
GTSRB_DATASET = './datasets/gtsrb_subset/test/'
TRAINS_ONTOLOGY = "./ontologies/XTRAINS.owl"
GTSRB_ONTOLOGY = "./ontologies/traffic_signals.owl"

MAIN_NN_TRAINS_A = './main_nns/Type_A_0.9935_ConvExp16_t20000_v5000.hdf5'
MAIN_NN_TRAINS_B = './main_nns/Type_B_0.986_ConvExp7_t20000_v5000.hdf5'
MAIN_NN_TRAINS_C = './main_nns/Type_C_0.9875_ConvExp19_t20000_v5000.hdf5'
MAIN_NN_GTSRB = './main_nns/SignClass_0.9990498812351544_MobileNetV2Exp_t33327_v5882_s10.hdf5'


def find_whole_word(w):
    return re.compile(r'\b({0})\b'.format(w)).search


def generate_classification_trains(path_to_nn, path_to_input):
    script = './scripts/classify.py -nn {} -i {}'.format(path_to_nn, path_to_input)
    os.system("python " + script)


def generate_classification_gtsrb(path_to_nn, path_to_input, just_class):
    script = './scripts/classify_gtsrb.py -nn {} -i {} -c {}'.format(path_to_nn, path_to_input, just_class)
    os.system("python " + script)


def generate_mapping_nns_trains(path_to_nn, concepts):
    concepts_text = concepts[0]
    for concept in concepts[1:]:
        concepts_text = concepts_text + " " + concept
    script = './scripts/generate_mapping_nns.py -nn {} -c {}'.format(path_to_nn, concepts_text)
    os.system("python " + script)


def generate_mapping_nns_gtsrb(path_to_nn, concepts):
    concepts_text = concepts[0]
    for concept in concepts[1:]:
        concepts_text = concepts_text + " " + concept
    script = './scripts/generate_mapping_nns_gtsrb.py -nn {} -c {}'.format(path_to_nn, concepts_text)
    os.system("python " + script)


def generate_observations_trains(path_to_nn, path_to_input, concepts):
    concepts_text = concepts[0]
    for concept in concepts[1:]:
        concepts_text = concepts_text + " " + concept
    script = './scripts/generate_observations.py -nn {} -i {} -c {}'.format(path_to_nn, path_to_input, concepts_text)
    os.system("python " + script)


def generate_observations_gtsrb(path_to_nn, path_to_input, concepts):
    concepts_text = concepts[0]
    for concept in concepts[1:]:
        concepts_text = concepts_text + " " + concept
    script = './scripts/generate_observations_gtsrb.py -nn {} -i {} -c {}'.format(path_to_nn, path_to_input, concepts_text)
    os.system("python " + script)


def generate_justifications(path_to_ontology, path_to_observations, path_to_output):
    script = './scripts/Justifier.jar {} {} {}'.format(path_to_ontology, path_to_observations, path_to_output)
    os.system("java -jar " + script)


class Session:
    TRAINS_CONCEPTS_MAP = {
        u"\u2203" "has.EmptyWagon": "EmptyWagon",
        u"\u2203" "has.PassengerCar": "PassengerCar",
        "LongFreightTrain": "LongFreightTrain",
        u"\u2203" "has.OpenRoofCar": "OpenRoof",

        u"\u2203" "has.FreightWagon": "FreightWagon",
        u"\u2203" "has.ReinforcedCar": "ReinforcedCar",
        "LongTrain": "LongTrain",
        "RuralTrain": "RuralTrain",

        u"\u2203" "has.LongWagon": "LongWagon",
        "EmptyTrain": "EmptyTrain",
        "MixedTrain": "MixedTrain",
        "WarTrain": "WarTrain",

        "": "",  # "LongPassengerTrain": "LongPassengerTrain",
        "FreightTrain": "FreightTrain",
        "PassengerTrain": "PassengerTrain",
        "": "",  # u"\u2203" "has.ThreeWheelsWagon": "ThreeWheelsWagon",
    }

    GTSRB_CONCEPTS_MAP = {
        u"\u2203" "hasShape.Triangle": "Triangular_Shape",
        u"\u2203" "hasShape.Octagon": "Octagonal_Shape",
        u"\u2203" "hasShape.Circle": "Circular_Shape",
        u"\u2203" "hasGround.Red": "Red_Ground",
        u"\u2203" "hasSymbol." u"\u22A4": "Symbol",
        u"\u2203" "hasBar.White": "White_Bar",
        u"\u2203" "hasGround.(White " u"\u2294" " Yellow)": "White_Ground",
        u"\u2203" "hasBorder.Red": "Red_Border",
        "StartProhibition": "Start_Prohibition",
        u"\u2203" "hasSymbol.NoEntryGoods": "Symbol_NoEntryGoods",
        u"\u2203" "hasBar." u"\u22A4": "Bar",
        u"\u2203" "hasSymbol.Overtaking": "Symbol_Overtaking",
        u"\u2203" "hasSymbol.OvertakingGoods": "Symbol_OvertakingGoods",
        u"\u2203" "hasSymbol.Speed20": "Symbol_Speed20",
        u"\u2203" "hasSymbol.Speed30": "Symbol_Speed30",
        u"\u2203" "hasSymbol.Speed50": "Symbol_Speed50",
        u"\u2203" "hasSymbol.Speed60": "Symbol_Speed60",
        u"\u2203" "hasSymbol.Speed70": "Symbol_Speed70",
        u"\u2203" "hasSymbol.Speed80": "Symbol_Speed80",
        u"\u2203" "hasSymbol.Speed100": "Symbol_Speed100",
        u"\u2203" "hasSymbol.Speed120": "Symbol_Speed120",
        u"\u2203" "hasBorder." u"\u22A4": "Border",
        u"\u2203" "hasBorder.Black": "Black_Border",
        u"\u2203" "hasBar.Black": "Black_Bar",
        "D1a1": "D1a1",
        "D1a4": "D1a4",
        "D1a5": "D1a5",
        "D1a6": "D1a6",
        "D1a7": "D1a7",
        "D2a1": "D2a1",
        "D2a2": "D2a2",
        "D3": "D3",
        u"\u2203" "hasSymbol.White": "White_Symbol",
        u"\u2203" "hasSymbol.Stop": "Symbol_Stop",
        u"\u2203" "hasShape.Diamond": "Diamond_Shape",
        u"\u2203" "hasGround.(Yellow " u"\u2294" " Orange)": "Yellow_Ground",
        u"\u2203" "hasBorder.White": "White_Border",
        u"\u2203" "hasSymbol.Black": "Black_Symbol",
    }
    GTSRB_CONCEPTS_MAP_A = {
        u"\u2203" "hasShape.Triangle": "Triangular_Shape",
        u"\u2203" "hasGround.(White " u"\u2294" " Yellow)": "White_Ground",
        u"\u2203" "hasBorder.Red": "Red_Border",
        u"\u2203" "hasSymbol." u"\u22A4": "Symbol",

        "": "",
        u"\u2203" "hasShape.Octagon": "Octagonal_Shape",
        u"\u2203" "hasBorder." u"\u22A4": "Border",
    }
    GTSRB_CONCEPTS_MAP_B = {
        u"\u2203" "hasShape.Triangle": "Triangular_Shape",
        u"\u2203" "hasGround.(White " u"\u2294" " Yellow)": "White_Ground",
        u"\u2203" "hasBorder.Red": "Red_Border",
        u"\u2203" "hasSymbol." u"\u22A4": "Symbol",
        u"\u2203" "hasShape.Octagon": "Octagonal_Shape",
        u"\u2203" "hasGround.Red": "Red_Ground",
        u"\u2203" "hasSymbol.White": "White_Symbol",
        u"\u2203" "hasSymbol.Stop": "Symbol_Stop",
        u"\u2203" "hasShape.Diamond": "Diamond_Shape",
        u"\u2203" "hasGround.(Yellow " u"\u2294" " Orange)": "Yellow_Ground",
        u"\u2203" "hasBorder.White": "White_Border",
        u"\u2203" "hasSymbol.Black": "Black_Symbol",
    }
    GTSRB_CONCEPTS_MAP_C = {
        u"\u2203" "hasShape.Circle": "Circular_Shape",
        u"\u2203" "hasGround.Red": "Red_Ground",
        u"\u2203" "hasSymbol." u"\u22A4": "Symbol",
        u"\u2203" "hasBar.White": "White_Bar",
        u"\u2203" "hasGround.(White " u"\u2294" " Yellow)": "White_Ground",
        u"\u2203" "hasBorder.Red": "Red_Border",
        "StartProhibition": "Start_Prohibition",
        u"\u2203" "hasSymbol.NoEntryGoods": "Symbol_NoEntryGoods",
        u"\u2203" "hasBar." u"\u22A4": "Bar",
        u"\u2203" "hasSymbol.Overtaking": "Symbol_Overtaking",
        u"\u2203" "hasSymbol.OvertakingGoods": "Symbol_OvertakingGoods",
        u"\u2203" "hasSymbol.Speed20": "Symbol_Speed20",
        u"\u2203" "hasSymbol.Speed30": "Symbol_Speed30",
        u"\u2203" "hasSymbol.Speed50": "Symbol_Speed50",
        u"\u2203" "hasSymbol.Speed60": "Symbol_Speed60",
        u"\u2203" "hasSymbol.Speed70": "Symbol_Speed70",
        u"\u2203" "hasSymbol.Speed80": "Symbol_Speed80",
        u"\u2203" "hasSymbol.Speed100": "Symbol_Speed100",
        u"\u2203" "hasSymbol.Speed120": "Symbol_Speed120",
        u"\u2203" "hasBorder." u"\u22A4": "Border",
        "": "",
        u"\u2203" "hasBorder.Black": "Black_Border",
        u"\u2203" "hasBar.Black": "Black_Bar",
    }
    GTSRB_CONCEPTS_MAP_D = {
        "D1a1": "D1a1",
        "D1a4": "D1a4",
        "D1a5": "D1a5",
        "D1a6": "D1a6",
        "D1a7": "D1a7",
        "D2a1": "D2a1",
        "D2a2": "D2a2",
        "D3": "D3",
        '': '',
        u"\u2203" "hasShape.Circle": "Circular_Shape",
        u"\u2203" "hasSymbol.White": "White_Symbol",
    }

    TRAINS_CONCEPTS_TO_OBSERVATION = {
        '(TypeA)': 'a type A train',
        '(TypeB)': 'a type B train',
        '(TypeC)': 'a type C train',

        '(has some LongWagon)': 'a train with a long wagon',
        '(has some PassengerCar)': 'a train with a passenger car',
        '(has some FreightWagon)': 'a train with a freight car',
        '(has some EmptyWagon)': 'a train with an empty wagon',

        '(LongTrain)': 'a long train',
        '(WarTrain)': 'a war train',
        '(PassengerTrain)': 'a passenger train',
        '(LongPassengerTrain)': 'a long passenger train',

        '(FreightTrain)': 'a freight train',
        '(LongFreightTrain)': 'a long freight train',
        '(EmptyTrain)': 'an empty train',
        '(RuralTrain)': 'a rural train',

        '(MixedTrain)': 'a mixed train',
        '(has some OpenRoofCar)': 'a train with an open roof car',
        '(has some ReinforcedCar)': 'a train with a reinforced car',
        '(has some ThreeWheelsWagon)': 'a train with a wagon with three wheels',
    }
    GTSRB_CONCEPTS_TO_OBSERVATION = {
        '(A)': 'a danger warning sign (class A)',
        '(B)': 'a priority sign (class B)',
        '(C)': 'a prohibitory or restrictive sign (class C)',
        '(D)': 'a mandatory sign (class D)',
        '(hasShape some Circle)': 'a sign with a circular shape',
        '(hasGround some Red)': 'a sign with a red ground',
        '(hasSymbol some)': 'a sign with a symbol',
        '(hasSymbol some Thing)': 'a sign with a symbol',
        '(hasBar some White)': 'a sign with a white bar',
        '(hasGround some White)': 'a sign with a white ground',
        '(hasBorder some Red)': 'a sign with a red border',
        '(StartProhibition)': 'a sign starting a prohibition',
        '(hasSymbol some NoEntryGoods)': 'a sign with a symbol prohibiting access or entry for goods vehicles',
        '(hasBar some)': 'a sign with a bar',
        '(hasBar some Thing)': 'a sign with a bar',
        '(hasSymbol some Overtaking)': 'a sign with a symbol prohibiting vehicles from overtaking',
        '(hasSymbol some OvertakingGoods)': 'a sign with a symbol prohibiting vehicles from overtaking for goods vehicles',
        '(hasSymbol some Speed20)': 'a sign with a symbol indicating a maximum speed of 20',
        '(hasSymbol some Speed30)': 'a sign with a symbol indicating a maximum speed of 30',
        '(hasSymbol some Speed50)': 'a sign with a symbol indicating a maximum speed of 50',
        '(hasSymbol some Speed60)': 'a sign with a symbol indicating a maximum speed of 60',
        '(hasSymbol some Speed70)': 'a sign with a symbol indicating a maximum speed of 70',
        '(hasSymbol some Speed80)': 'a sign with a symbol indicating a maximum speed of 80',
        '(hasSymbol some Speed100)': 'a sign with a symbol indicating a maximum speed of 100',
        '(hasSymbol some Speed120)': 'a sign with a symbol indicating a maximum speed of 120',
        '(hasBorder some)': 'a sign with a border',
        '(hasBorder some Thing)': 'a sign with a border',
        '(hasBorder some Black)': 'a sign with a black border',
        '(hasBar some Black)': 'a sign with a black bar',
        '(hasShape some Triangle)': 'a sign with a triangular shape',
        '(hasShape some Octagon)': 'a sign with a octangular shape',
        '(hasSymbol some White)': 'a sign with a white symbol',
        '(hasSymbol some Stop)': 'a sign with a STOP symbol',
        '(hasShape some Diamond)': 'a sign with a diamond shape',
        '(hasGround some Yellow)': 'a sign with a yellow ground',
        '(hasBorder some White)': 'a sign with a white border',
        'D1a1': 'a mandatory straight direction sign (D1a1)',
        'D1a4': 'a mandatory left turn direction sign (D1a4)',
        'D1a5': 'a mandatory right turn direction sign (D1a5)',
        'D1a6': 'a mandatory straight or left turn direction sign (D1a6)',
        'D1a7': 'a mandatory straight or right turn direction sign (D1a7)',
        'D2a1': 'a pass on the left direction sign (D2a1)',
        'D2a2': 'a pass on the right direction sign (D2a2)',
        'D3': 'a compulsory roundabout sign (D3)',
        '(hasSymbol some Black)': 'a sign with a black symbol',
    }

    TRAINS_CONCEPTS_TO_ONTO = {
        'TypeA': 'of type A',
        'TypeB': 'of type B',
        'TypeC': 'of type C',

        'has some LongWagon': 'having a long wagon',
        'has some PassengerCar': 'having a passenger car',
        'has some FreightWagon': 'having a freight car',
        'has some EmptyWagon': 'having an empty wagon',

        'LongPassengerTrain': 'that are long passenger trains',
        'LongTrain': 'that are long',
        'WarTrain': 'that are war trains',
        'PassengerTrain': 'that are passenger trains',

        'LongFreightTrain': 'that are long freight trains',
        'FreightTrain': 'that are freight trains',
        'EmptyTrain': 'that are empty',
        'RuralTrain': 'that are rural trains',

        'MixedTrain': 'that are mixed trains',
        'has some OpenRoofCar': 'having an open roof car',
        'has some ReinforcedCar': 'having a reinforced car',
        'has some ThreeWheelsWagon': 'having a wagon with three wheels',
    }
    GTSRB_CONCEPTS_TO_ONTO = {
        'StartProhibition': 'having a sign starting a prohibition',
        'hasGround some (White or Yellow)': 'having a white or yellow ground',
        'hasGround some Red': 'having a red ground',
        'hasGround some White': 'having a white ground',
        'hasGround some Yellow': 'having a yellow ground',
        'hasBar some White': 'having a white bar',
        'hasBar some Black': 'having a black bar',
        'hasBar some Thing': 'having a bar',
        'hasBar some': 'having a bar',
        'hasBorder some Red': 'having a red border',
        'hasBorder some Black': 'having a black border',
        'hasBorder some White': 'having a white border',
        'hasBorder some Thing': 'having a border',
        'hasBorder some': 'having a border',
        'hasShape some Circle': 'having a circular shape',
        'hasShape some Triangle': 'having a triangular shape',
        'hasShape some Octagon': 'having a octangular shape',
        'hasShape some Diamond': 'having a diamond shape',
        'hasSymbol some White': 'having a white symbol',
        'hasSymbol some Stop': 'having a STOP symbol',
        'hasSymbol some Black': 'having a black symbol',
        'hasSymbol some NoEntryGoods': 'having a symbol prohibiting access or entry for goods vehicles',
        'hasSymbol some Overtaking': 'having a symbol prohibiting vehicles from overtaking',
        'hasSymbol some OvertakingGoods': 'having a symbol prohibiting vehicles from overtaking for goods vehicles',
        'hasSymbol some Speed20': 'having a symbol indicating a maximum speed of 20',
        'hasSymbol some Speed30': 'having a symbol indicating a maximum speed of 30',
        'hasSymbol some Speed50': 'having a symbol indicating a maximum speed of 50',
        'hasSymbol some Speed60': 'having a symbol indicating a maximum speed of 60',
        'hasSymbol some Speed70': 'having a symbol indicating a maximum speed of 70',
        'hasSymbol some Speed80': 'having a symbol indicating a maximum speed of 80',
        'hasSymbol some Speed100': 'having a symbol indicating a maximum speed of 100',
        'hasSymbol some Speed120': 'having a symbol indicating a maximum speed of 120',
        'hasSymbol some Thing': 'having a symbol',
        'hasSymbol some': 'having a symbol',
        'D1a1': 'of a mandatory straight direction sign (D1a1)',
        'D1a4': 'of a mandatory left turn direction sign (D1a4)',
        'D1a5': 'of a mandatory right turn direction sign (D1a5)',
        'D1a6': 'of a mandatory straight or left turn direction sign (D1a6)',
        'D1a7': 'of a mandatory straight or right turn direction sign (D1a7)',
        'D2a1': 'of a pass on the left direction sign (D2a1)',
        'D2a2': 'of a pass on the right direction sign (D2a2)',
        'D3': 'of a compulsory roundabout sign (D3)',
        'A28': 'of type A28',
        'A29': 'of type A29',
        'A_b': 'of type A_b',
        'A_a': 'of type A_a',
        'A': 'of danger warning type (class A)',
        'B': 'of priority type (class B)',
        'C': 'of prohibitory or restrictive type (class C)',
        'D': 'of mandatory type (class D)',
    }

    def __init__(self, dataset):
        self.dataset = dataset

        if self.dataset == TRAINS_DATASET:
            self.main_concept = MAIN_NN_TRAINS_A
        elif self.dataset == GTSRB_DATASET:
            self.main_concept = "A"

        self.image_index = 0
        self.image_files = glob.glob(self.dataset + '*')
        print(self.image_files)
        self.curr_image = None
        self.curr_image_filename = None

    def set_main_concept(self, concept):
        self.main_concept = concept

    def get_current_image_file(self):
        return self.image_files[self.image_index]

    def get_current_image(self, h=152, w=152):
        print(self.image_files[self.image_index])
        self.curr_image_filename = self.image_files[self.image_index]
        self.curr_image = Image.open(self.curr_image_filename)
        img = self.curr_image.resize((h, w))
        return ImageTk.PhotoImage(img)

    def get_next_image(self):
        self.image_index += 1
        self.image_index = self.image_index % len(self.image_files)
        return self.get_current_image()

    def get_prev_image(self):
        self.image_index -= 1
        self.image_index = self.image_index % len(self.image_files)
        return self.get_current_image()

    def set_current_image(self, path, h=152, w=152):
        print(path)
        self.curr_image_filename = path
        self.curr_image = Image.open(self.curr_image_filename)
        img = self.curr_image.resize((h, w))
        return ImageTk.PhotoImage(img)

    def get_possible_concept_list(self):
        if self.dataset == TRAINS_DATASET:
            return self.TRAINS_CONCEPTS_MAP.keys()
        elif self.dataset == GTSRB_DATASET:
            if self.main_concept == "A":
                return self.GTSRB_CONCEPTS_MAP_A.keys()
            elif self.main_concept == "B":
                return self.GTSRB_CONCEPTS_MAP_B.keys()
            elif self.main_concept == "C":
                return self.GTSRB_CONCEPTS_MAP_C.keys()
            elif self.main_concept == "D":
                return self.GTSRB_CONCEPTS_MAP_D.keys()
        return []


class JustificationPlatform(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self._frame = None
        self.title(APP_NAME + ' - Visual Demo')
        self.geometry('740x500')

        # define application fonts
        self.title_font = tkfont.Font(family='Helvetica', size=18, weight='bold')
        self.m_font = tkfont.Font(family='Helvetica', size=14)
        self.s_font = tkfont.Font(family='Helvetica', size=12)
        self.xs_font = tkfont.Font(family='Helvetica', size=10)
        self.xxs_font = tkfont.Font(family='Helvetica', size=8)

        # define application folders
        self.FOLDER_MAPPING_NNS = './temp/mapping_nns/'
        self.FOLDER_OBSERVATIONS = './temp/observations/'
        self.FOLDER_JUSTIFICATIONS = './temp/justifications/'

        # if needed create folders to store the results
        os.makedirs(self.FOLDER_MAPPING_NNS, exist_ok=True)
        os.makedirs(self.FOLDER_OBSERVATIONS, exist_ok=True)
        os.makedirs(self.FOLDER_JUSTIFICATIONS, exist_ok=True)
        self.empty_folder(self.FOLDER_MAPPING_NNS)
        self.empty_folder(self.FOLDER_OBSERVATIONS)
        self.empty_folder(self.FOLDER_JUSTIFICATIONS)

        self.switch_frame(MainPage)

    @staticmethod
    def empty_folder(dir_path):
        for filename in os.listdir(dir_path):
            filepath = os.path.join(dir_path, filename)
            os.remove(filepath)

    def switch_frame(self, frame_class):
        """Destroys current frame and replaces it with a new one."""
        new_frame = frame_class(self)
        if self._frame is not None:
            self._frame.destroy()
        self._frame = new_frame
        self._frame.pack(fill=tk.BOTH, expand=1)


class MainPage(tk.Frame):
    def __init__(self, master):
        self.master = master
        tk.Frame.__init__(self, self.master)

        ### GRID ###
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(0, weight=0)
        self.rowconfigure(1, weight=0)
        self.rowconfigure(2, weight=1)
        self.rowconfigure(3, weight=0)

        ### DATASET ###
        dataset_frame = tk.LabelFrame(self, text='Dataset:', font=self.master.s_font, padx=5, pady=5)
        dataset_frame.grid(row=0, column=0, columnspan=2, padx=10, pady=10, sticky=tk.E + tk.W + tk.N)

        self.dataset_var = tk.StringVar(value=TRAINS_DATASET)
        r_trains = tk.Radiobutton(dataset_frame, text="Explainable Abstract Trains", padx=10, font=self.master.xs_font,
                                  variable=self.dataset_var, value=TRAINS_DATASET, command=self.trains_sess)
        r_gtsrb = tk.Radiobutton(dataset_frame, text="German Traffic Sign Recognition Benchmark", padx=10, font=self.master.xs_font,
                                 variable=self.dataset_var, value=GTSRB_DATASET, command=self.gtsrb_sess)
        r_trains.grid(row=0, column=0, padx=7)
        r_gtsrb.grid(row=0, column=1, padx=7)

        ### MAIN CONCEPT ###
        self.nn_frame = tk.LabelFrame(self, text='Main Network:', font=self.master.s_font, padx=5, pady=5)
        self.nn_frame.grid(row=1, column=0, columnspan=1, padx=10, pady=10, sticky=tk.E+tk.W+tk.N+tk.S)
        self.set_trains_main_concept()

        ### INPUT ###
        self.dataset_frame_image = tk.LabelFrame(self, text='Input:', font=self.master.s_font, padx=5, pady=5)
        self.dataset_frame_image.grid(row=1, column=1, columnspan=1, padx=10, pady=10, sticky=tk.E+tk.W+tk.N+tk.S)
        self.input_img_label = tk.Label(master=self.dataset_frame_image, anchor='w', justify=tk.LEFT)
        self.input_img_label.grid(row=0, column=0, rowspan=3)
        left_button = tk.Button(self.dataset_frame_image, text='<<', font=self.master.s_font, padx=5, command=self.prev_image)
        right_button = tk.Button(self.dataset_frame_image, text='>>', font=self.master.s_font, padx=5, command=self.next_image)
        browse_button = tk.Button(self.dataset_frame_image, text='Browse', font=self.master.s_font, padx=5, command=self.open_image)

        self.dataset_frame_image.columnconfigure(0, weight=0)
        self.dataset_frame_image.columnconfigure(1, weight=1)

        left_button.grid(row=0, column=1, columnspan=1, padx=30, sticky=tk.E+tk.W)
        right_button.grid(row=1, column=1, columnspan=1, padx=30, sticky=tk.E+tk.W)
        browse_button.grid(row=2, column=1, columnspan=1, padx=30, sticky=tk.E+tk.W)

        ### CONCEPTS TO EXTRACT ###
        self.extract_frame = tk.LabelFrame(self, text='Mapping Concepts:', font=self.master.s_font, padx=5, pady=5)
        self.extract_frame.grid(row=2, column=0, columnspan=2, padx=10, pady=10, sticky=tk.E + tk.W + tk.N + tk.S)
        self.extract_frame.columnconfigure(0, weight=1)
        self.extract_frame.rowconfigure(0, weight=1)

        ### SESSION ###
        self.trains_sess()

        ### LAST OPTIONS ###
        self.new_mapping_nns = tk.BooleanVar(value=True)
        new_mapping_nns_button = tk.Checkbutton(self, text="Build new mapping networks", variable=self.new_mapping_nns,
                                                state=tk.DISABLED)  # TODO allow to use pretrained mapping networks
        new_mapping_nns_button.grid(row=3, column=0, padx=10, pady=(0, 7), sticky=tk.W+tk.N+tk.S)

        self.justify_button = tk.Button(self, text='Justify', font=self.master.s_font, padx=5, command=self.justify)
        self.justify_button.grid(row=3, column=1, padx=(60, 11), pady=(0, 7), sticky=tk.W+tk.E+tk.N)

    def trains_sess(self):
        self.session = Session(TRAINS_DATASET)

        self.temp_img = self.session.get_current_image()
        self.input_img_label.configure(image=self.temp_img)

        self.nn_frame.config(text='Main Network:')
        self.set_trains_main_concept()
        self.set_extract_concepts()

    def gtsrb_sess(self):
        self.session = Session(GTSRB_DATASET)

        self.temp_img = self.session.get_current_image()
        self.input_img_label.configure(image=self.temp_img)

        self.nn_frame.config(text='Output Concept:')
        self.set_gtsrb_main_concept()
        self.set_extract_concepts()

    def set_trains_main_concept(self):
        for label in self.nn_frame.winfo_children():
            label.destroy()

        self.main_concept_var = tk.StringVar(value=MAIN_NN_TRAINS_A)
        r_A = tk.Radiobutton(self.nn_frame, text="Type A", padx=10, font=self.master.xs_font, variable=self.main_concept_var,
                             value=MAIN_NN_TRAINS_A,
                             command=lambda: self.session.set_main_concept(self.main_concept_var.get()))
        r_B = tk.Radiobutton(self.nn_frame, text="Type B", padx=10, font=self.master.xs_font, variable=self.main_concept_var,
                             value=MAIN_NN_TRAINS_B,
                             command=lambda: self.session.set_main_concept(self.main_concept_var.get()))
        r_C = tk.Radiobutton(self.nn_frame, text="Type C", padx=10, font=self.master.xs_font, variable=self.main_concept_var,
                             value=MAIN_NN_TRAINS_C,
                             command=lambda: self.session.set_main_concept(self.main_concept_var.get()))
        r_A.grid(row=0, column=0, columnspan=1, padx=10, pady=10, sticky=tk.E + tk.W + tk.N)#.pack(side='top', anchor='w')
        r_B.grid(row=1, column=0, columnspan=1, padx=10, pady=10, sticky=tk.E + tk.W + tk.N)#.pack(side='top', anchor='w')
        r_C.grid(row=2, column=0, columnspan=1, padx=10, pady=10, sticky=tk.E + tk.W + tk.N)#.pack(side='top', anchor='w')
        self.nn_frame.update()

    def _change_gtsrb_concept(self, concept):
        self.session.set_main_concept(concept)
        self.set_extract_concepts()

    def set_gtsrb_main_concept(self):
        for label in self.nn_frame.winfo_children():
            label.destroy()

        self.main_concept_var = tk.StringVar(value="A")
        r_A = tk.Radiobutton(self.nn_frame, text="Category A", padx=5, font=self.master.xs_font, variable=self.main_concept_var,
                             value="A", command=lambda: self._change_gtsrb_concept(self.main_concept_var.get()))
        r_B = tk.Radiobutton(self.nn_frame, text="Category B", padx=5, font=self.master.xs_font, variable=self.main_concept_var,
                             value="B", command=lambda: self._change_gtsrb_concept(self.main_concept_var.get()))
        r_C = tk.Radiobutton(self.nn_frame, text="Category C", padx=5, font=self.master.xs_font, variable=self.main_concept_var,
                             value="C", command=lambda: self._change_gtsrb_concept(self.main_concept_var.get()))
        r_D = tk.Radiobutton(self.nn_frame, text="Category D", padx=5, font=self.master.xs_font, variable=self.main_concept_var,
                             value="D", command=lambda: self._change_gtsrb_concept(self.main_concept_var.get()))
        r_A.grid(row=0, column=0, columnspan=1, padx=10, pady=5, sticky=tk.E + tk.W + tk.N)#.pack(side='top', anchor='w')
        r_B.grid(row=1, column=0, columnspan=1, padx=10, pady=5, sticky=tk.E + tk.W + tk.N)#.pack(side='top', anchor='w')
        r_C.grid(row=2, column=0, columnspan=1, padx=10, pady=5, sticky=tk.E + tk.W + tk.N)#.pack(side='top', anchor='w')
        r_D.grid(row=3, column=0, columnspan=1, padx=10, pady=5, sticky=tk.E + tk.W + tk.N)#.pack(side='top', anchor='w')
        self.nn_frame.update()

    def prev_image(self):
        self.temp_img = self.session.get_prev_image()
        self.input_img_label.configure(image=self.temp_img)

    def next_image(self):
        self.temp_img = self.session.get_next_image()
        self.input_img_label.configure(image=self.temp_img)

    def open_image(self):
        """Opens a file dialog to select an image file."""

        if self.session.dataset == TRAINS_DATASET:
            filetypes = [("PNG Files", "*.png"), ("PPM Files", "*.ppm"), ("BMP Files", "*.bmp"),
                         ("JPEG Files", "*.jpeg *.jpg"), ("TIFF Files", "*.tiff *.tif"), ("All Files", "*.*")]
        elif self.session.dataset == GTSRB_DATASET:
            filetypes = [("PPM Files", "*.ppm"), ("PNG Files", "*.png"), ("BMP Files", "*.bmp"),
                         ("JPEG Files", "*.jpeg *.jpg"), ("TIFF Files", "*.tiff *.tif"), ("All Files", "*.*")]
        else: filetypes = [("All Files", "*.*")]

        path = tkfiledialog.askopenfilename(initialdir=self.session.dataset, title="Select a Model", filetypes=filetypes)

        if isinstance(path, str) and path != '':
            self.temp_img = self.session.set_current_image(path)
            self.input_img_label.configure(image=self.temp_img)

    def set_extract_concepts(self):
        for label in self.extract_frame.winfo_children():
            label.destroy()

        self.checkboxes = []
        self.checkboxes_values = []

        for i, concept in enumerate(self.session.get_possible_concept_list()):
            if concept == "": continue
            column = i % 4
            row = i // 4

            var = tk.StringVar()

            c = tk.Checkbutton(self.extract_frame, variable=var, onvalue=concept, offvalue='', text=concept)
            c.grid(row=row, column=column, sticky="W")

            self.checkboxes.append(c)
            self.checkboxes_values.append(var)

            self.extract_frame.rowconfigure(row, weight=1)
            self.extract_frame.columnconfigure(column, weight=1)

    def justify(self):
        self.concepts_to_extract_pretty = ([v.get() for v in self.checkboxes_values if v.get() != ''])

        if self.session.dataset == TRAINS_DATASET:
            self.concepts_to_extract = [self.session.TRAINS_CONCEPTS_MAP[v] for v in self.concepts_to_extract_pretty]
            self.nn_filename = self.session.main_concept
            self.output_concept = self.nn_filename.split('/')[-1][:6]  # a bit hacked, gets the output concept from the nn filename
        elif self.session.dataset == GTSRB_DATASET:
            self.concepts_to_extract = [self.session.GTSRB_CONCEPTS_MAP[v] for v in self.concepts_to_extract_pretty]
            self.nn_filename = MAIN_NN_GTSRB
            self.output_concept = self.session.main_concept

        self.input_filename = self.session.curr_image_filename

        if self.nn_filename is None:
            tkmessagebox.showwarning(APP_NAME, "To proceed select a neural network to analyze.")
        elif self.input_filename is None:
            tkmessagebox.showwarning(APP_NAME, "To proceed select an image file - an input - to feed the neural network.")
        elif len(self.concepts_to_extract) == 0:
            tkmessagebox.showwarning(APP_NAME, "To proceed select at least a concept to extract.")
        else:
            self.justify_button.configure(state=tk.DISABLED)
            if self.session.dataset == TRAINS_DATASET:
                self.p = multiprocessing.Process(target=generate_classification_trains, args=(self.nn_filename, self.input_filename))
                self.p.start()
                self.after(200, self.check_classification)
            elif self.session.dataset == GTSRB_DATASET:
                self.p = multiprocessing.Process(target=generate_classification_gtsrb,
                                                 args=(self.nn_filename, self.input_filename, self.session.main_concept))
                self.p.start()
                self.after(200, self.check_classification)

    def check_classification(self):
        if not self.p.is_alive():
            # process is done, update the GUI
            filename = './temp/observations/classification.txt'

            with open(filename, 'r') as classification_file:
                self.classification_report = classification_file.read()
                classification_file.close()

            # proceed to the next step
            if self.session.dataset == TRAINS_DATASET:
                self.p = multiprocessing.Process(target=generate_mapping_nns_trains, args=(self.nn_filename, self.concepts_to_extract))
            elif self.session.dataset == GTSRB_DATASET:
                self.p = multiprocessing.Process(target=generate_mapping_nns_gtsrb, args=(self.nn_filename, self.concepts_to_extract))
            self.p.start()

            self.after(200, self.check_mapping_nns)

        else:
            # process not yet finished, check again later
            self.after(200, self.check_classification)

    def check_mapping_nns(self):
        if not self.p.is_alive():
            # process is done, update the GUI

            # proceed to the next step
            if self.session.dataset == TRAINS_DATASET:
                self.p = multiprocessing.Process(target=generate_observations_trains, args=(self.nn_filename, self.input_filename, self.concepts_to_extract))
            elif self.session.dataset == GTSRB_DATASET:
                self.p = multiprocessing.Process(target=generate_observations_gtsrb, args=(self.nn_filename, self.input_filename, self.concepts_to_extract))
            self.p.start()

            self.after(200, self.check_observations)

        else:
            # process not yet finished, check again later
            self.after(200, self.check_mapping_nns)

    def check_observations(self):
        if not self.p.is_alive():
            # process is done, update the GUI
            filename = './temp/observations/observation.txt'

            if self.session.dataset == TRAINS_DATASET:
                concepts_to_observation = self.session.TRAINS_CONCEPTS_TO_OBSERVATION
                ontology = TRAINS_ONTOLOGY
                just_checker = self.check_justification_trains
            elif self.session.dataset == GTSRB_DATASET:
                concepts_to_observation = self.session.GTSRB_CONCEPTS_TO_OBSERVATION
                ontology = GTSRB_ONTOLOGY
                just_checker = self.check_justification_gtsrb

            with open(filename, 'r') as observation_file:
                observation_text = observation_file.read()
                observation_file.close()

            observation_text = observation_text.split('\n')
            observation_text = observation_text[1:]
            text = 'The mapping networks performed the following observations:\n'

            for obs in observation_text:
                if obs != '':
                    obs = obs.split(',')
                    print(obs)
                    concept = obs[0].split(': ')[-1]
                    confidence = round(float(obs[1]), 4)
                    print(concept, confidence)
                    if 'not (' in concept:
                        concept = concept.split('not ')[-1]
                        text = text + '\tThe input does not contain {} with a degree of belief of {}.\n'.format(
                            concepts_to_observation[concept], str(confidence))
                    else:
                        text = text + '\tThe input contains {} with a degree of belief of {}.\n'.format(
                            concepts_to_observation[concept], str(confidence))

            self.observations_report = text
            print(self.observations_report)

            # proceed to the next step
            self.p = multiprocessing.Process(target=generate_justifications, args=(ontology, './temp/observations/observation.txt', './temp/justifications/justification.txt'))
            self.p.start()

            self.after(200, just_checker)
        else:
            # process not yet finished, check again later
            self.after(200, self.check_observations)

    def check_justification_trains(self):
        manchester_just_text = ''
        self.human_just_texts = []
        self.manchester_just_texts = []

        if not self.p.is_alive():
            # process is done, update the GUI
            filename = './temp/justifications/justification.txt'

            with open(filename, 'r') as justification_file:
                justification_text = justification_file.read()
                justification_file.close()

            justification_text = justification_text.strip().split('Found Justifications:')[-1]
            if justification_text == '':
                justification_text = 'No justifications were found for the output of the original network.'
                self.human_just_texts = [justification_text]
                self.manchester_just_texts = [justification_text]
            else:
                justifications = justification_text.split('Justification for ')[1:]
                for justification in justifications:
                    justification_parts = justification.split('\n')
                    justification_confidence = justification_parts[0].split('Degree of Belief: ')[-1]
                    justification_confidence = float(re.findall("\d+\.\d+", justification_confidence)[0])

                    justification_text = 'Justification for the output classification, with a degree of belief of {}.\n'.format(justification_confidence)
                    manchester_just_text = manchester_just_text + justification_text
                    classification_text = self.classification_report.split('an image ')[-1]
                    classification_text = classification_text[:-1]
                    justification_text = justification_text + 'The input was classified as an image {}, because:\n'.format(classification_text)
                    for phrase in justification_parts[1:]:
                        if '__input__ Type ' in phrase:
                            phrase = phrase.split('\t')[1:]
                            concept = phrase[0].split('Type ')[-1]
                            confidence = float(re.findall("\d+\.\d+", phrase[-1])[0])
                            confidence = round(confidence, 4)
                            print(concept, confidence)
                            manchester_just_text = manchester_just_text + '\t' + phrase[0] + " (degree of belief: " + str(confidence) + ')\n'
                            if 'not (' in concept:
                                concept = concept.split('not ')[-1]
                                phrase_text = '\tThe input does not contain {} with a degree of belief of {}.\n'.format(
                                    self.session.TRAINS_CONCEPTS_TO_OBSERVATION['({})'.format(concept)], str(confidence))
                            else:
                                phrase_text = '\tThe input contains {} with a degree of belief of {}.\n'.format(
                                    self.session.TRAINS_CONCEPTS_TO_OBSERVATION['({})'.format(concept)], str(confidence))

                        else:
                            # substitute concepts
                            phrase_text = phrase[1:]
                            manchester_just_text = manchester_just_text + '\t' + phrase_text + '\n'
                            for concept in self.session.TRAINS_CONCEPTS_TO_ONTO.keys():
                                _concept = '({})'.format(concept)
                                if concept in phrase_text:
                                    if _concept in phrase_text:
                                        phrase_text = phrase_text.replace(_concept, self.session.TRAINS_CONCEPTS_TO_ONTO[concept])
                                    else:
                                        phrase_text = phrase_text.replace(concept, self.session.TRAINS_CONCEPTS_TO_ONTO[concept])

                            sub_class_of = 'SubClassOf'
                            if sub_class_of in phrase_text:
                                phrase_text = phrase_text.replace(sub_class_of, 'are trains')

                            equivalent_to = 'EquivalentTo'
                            if equivalent_to in phrase_text:
                                phrase_text = phrase_text.replace(equivalent_to, 'are equivalent to trains')

                            phrase_text = '\tTrains ' + phrase_text
                            phrase_text = phrase_text + '.\n'

                        justification_text = justification_text + phrase_text

                    justification_text = justification_text + '\tTherefore, the input image was classified as {}.'.format(classification_text)
                    self.human_just_texts.append(justification_text)
                    self.manchester_just_texts.append(manchester_just_text)

            print('---\n')
            print('self.human_just_texts', self.human_just_texts)
            print('---\n')
            print('self.manchester_just_texts', self.manchester_just_texts)
            print('---\n')

            # proceed to the next step
            self.justify_button.configure(state=tk.NORMAL)
            self.justification_window(self.master)

        else:
            # process not yet finished, check again later
            self.after(200, self.check_justification_trains)

    def check_justification_gtsrb(self):
        manchester_just_text = ''
        self.human_just_texts = []
        self.manchester_just_texts = []

        if not self.p.is_alive():
            # process is done, update the GUI
            filename = './temp/justifications/justification.txt'

            with open(filename, 'r') as justification_file:
                justification_text = justification_file.read()
                justification_file.close()

            justification_text = justification_text.strip().split('Found Justifications:')[-1]
            if justification_text == '':
                justification_text = 'No justifications were found for the output of the original network.'
                self.human_just_texts = [justification_text]
                self.manchester_just_texts = [justification_text]
            else:
                justifications = justification_text.split('Justification for ')[1:]
                for justification in justifications:
                    justification_parts = justification.split('\n')
                    justification_confidence = justification_parts[0].split('Degree of Belief: ')[-1]
                    justification_confidence = float(re.findall("\d+\.\d+", justification_confidence)[0])

                    justification_text = 'Justification for the output classification, with a degree of belief of {}.\n'.format(justification_confidence)
                    manchester_just_text = manchester_just_text + justification_text
                    classification_text = self.classification_report.split('an image ')[-1]
                    classification_text = classification_text[:-1]
                    justification_text = justification_text + 'The input was classified as an image {}, because:\n'.format(classification_text)
                    for phrase in justification_parts[1:]:
                        if '__input__ Type ' in phrase:
                            phrase = phrase.split('\t')[1:]
                            concept = phrase[0].split('Type ')[-1]
                            confidence = float(re.findall("\d+\.\d+", phrase[-1])[0])
                            confidence = round(confidence, 4)
                            print(concept, confidence)
                            manchester_just_text = manchester_just_text + '\t' + phrase[0] + " (degree of belief: " + str(confidence) + ')\n'
                            if 'not (' in concept:
                                concept = concept.split('not ')[-1]
                                phrase_text = '\tThe input does not contain {} with a degree of belief of {}.\n'.format(
                                    self.session.GTSRB_CONCEPTS_TO_OBSERVATION['({})'.format(concept)], str(confidence))
                            else:
                                phrase_text = '\tThe input contains {} with a degree of belief of {}.\n'.format(
                                    self.session.GTSRB_CONCEPTS_TO_OBSERVATION['({})'.format(concept)], str(confidence))

                        else:
                            # substitute concepts
                            phrase_text = phrase[1:]
                            manchester_just_text = manchester_just_text + '\t' + phrase_text + '\n'
                            print(phrase_text)
                            for concept in self.session.GTSRB_CONCEPTS_TO_ONTO.keys():
                                _concept = '({})'.format(concept)

                                if find_whole_word(concept)(phrase_text):
                                    if _concept in phrase_text:
                                        phrase_text = phrase_text.replace(_concept, self.session.GTSRB_CONCEPTS_TO_ONTO[concept])
                                    else:
                                        x = find_whole_word(concept)(phrase_text)
                                        phrase_text = phrase_text[:x.start()] + self.session.GTSRB_CONCEPTS_TO_ONTO[concept] + phrase_text[x.end():]
                                    print('subs', concept, self.session.GTSRB_CONCEPTS_TO_ONTO[concept])
                                    print(phrase_text)

                                c = 'hasGround some (White or Yellow)'  # TODO phrases with () in middle don't work in previous method....
                                _c = '({})'.format(c)
                                if 'hasGround some (White or Yellow)' in phrase_text:
                                    if _c in phrase_text:
                                        phrase_text = phrase_text.replace(_c, self.session.GTSRB_CONCEPTS_TO_ONTO[c])
                                    else:
                                        phrase_text = phrase_text.replace(c, self.session.GTSRB_CONCEPTS_TO_ONTO[c])

                            sub_class_of = 'SubClassOf'
                            if sub_class_of in phrase_text:
                                phrase_text = phrase_text.replace(sub_class_of, 'are signs')

                            equivalent_to = 'EquivalentTo'
                            if equivalent_to in phrase_text:
                                phrase_text = phrase_text.replace(equivalent_to, 'are equivalent to signs')

                            phrase_text = '\tSigns ' + phrase_text
                            phrase_text = phrase_text + '.\n'

                        justification_text = justification_text + phrase_text

                    justification_text = justification_text + '\tTherefore, the input image was classified as {}.'.format(classification_text)
                    self.human_just_texts.append(justification_text)
                    self.manchester_just_texts.append(manchester_just_text)

            print('---\n')
            print('self.human_just_texts', self.human_just_texts)
            print('---\n')
            print('self.manchester_just_texts', self.manchester_just_texts)
            print('---\n')

            # proceed to the next step
            self.justify_button.configure(state=tk.NORMAL)
            self.justification_window(self.master)

        else:
            # process not yet finished, check again later
            self.after(200, self.check_justification_gtsrb)

    def justification_window(self, master):
        ### WINDOW ###
        win = tk.Toplevel()
        win.wm_title(APP_NAME + " - Justification")
        self.canvas = tk.Canvas(win, height=300, width=600)
        self.canvas.pack(side=tk.LEFT, fill="both", expand=True)
        scrollbar = tk.Scrollbar(win, command=self.canvas.yview)

        self.frame = tk.Frame(self.canvas, height=300, width=600)
        self.frame.pack(side=tk.LEFT, fill="both", expand=True)

        ### GRID ###
        self.frame.rowconfigure(0, weight=1)
        self.frame.rowconfigure(1, weight=0)

        ### JUSTIFICATION ###
        self.justification_label = tk.Label(self.frame, text=self.human_just_texts[0], font=self.master.xs_font, anchor='nw', justify=tk.LEFT)
        self.update()
        w = self.canvas.winfo_width() - 10  # substract scroll width
        self.justification_label.config(wraplength=w)
        self.justification_label.grid(row=0, column=0, columnspan=3, padx=5, pady=10, sticky=tk.N + tk.S + tk.E + tk.W)

        ### BUTTONS ###
        self.language_button_labels = ['Manchester OWL', 'Natural Language']
        self.language_button_state = False
        self.detail_button_labels = ['More Details', 'Less Details']
        self.more_detail = False
        self.language_button = tk.Button(self.frame, text=self.language_button_labels[self.language_button_state], width=15, font=self.master.xs_font, command=self.substituteSyntax)

        back_button = tk.Button(self.frame, text='Back', font=self.master.xs_font, padx=5, command=win.destroy)
        self.detail_button = tk.Button(self.frame, text=self.detail_button_labels[self.more_detail], font=self.master.xs_font, padx=5, command=self.substituteDetail)

        back_button.grid(row=1, column=0, columnspan=1, padx=5, pady=(15, 7))
        self.language_button.grid(row=1, column=1, columnspan=1, padx=5, pady=(15, 7))
        self.detail_button.grid(row=1, column=2, columnspan=1, padx=5, pady=(15, 7))


        scrollbar.pack(side=tk.RIGHT, fill='y')
        self.canvas.configure(yscrollcommand=scrollbar.set)
        self.canvas.bind('<Configure>', self.on_configure)
        self.canvas.bind('<Configure>', self.resize_frame)

        self.frame.update()

    def substituteSyntax(self):
        self.language_button_state = not self.language_button_state

        if self.language_button_labels[self.language_button_state] == 'Manchester OWL':
            self.justification_label.config(text=self.get_just_text())
        elif self.language_button_labels[self.language_button_state] == 'Natural Language':
            self.justification_label.config(text=self.get_just_text())

        w = self.winfo_width() - 10  # substract scroll width
        self.justification_label.config(wraplength=w)

        self.language_button.config(text=self.language_button_labels[self.language_button_state])

        self.frame.update()
        self.update()
        self.canvas.configure(scrollregion=self.canvas.bbox('all'))

    def substituteDetail(self):
        self.more_detail = not self.more_detail

        if self.language_button_labels[self.language_button_state] == 'Manchester OWL':
            self.justification_label.config(text=self.get_just_text())
        elif self.language_button_labels[self.language_button_state] == 'Natural Language':
            self.justification_label.config(text=self.get_just_text())

        w = self.winfo_width() - 10  # substract scroll width
        self.justification_label.config(wraplength=w)

        self.detail_button.config(text=self.detail_button_labels[self.more_detail])

        self.frame.update()
        self.update()
        self.canvas.configure(scrollregion=self.canvas.bbox('all'))

    def on_configure(self, event):
        # update scrollregion
        self.canvas.configure(scrollregion=self.canvas.bbox('all'))

    def resize_frame(self, e):
        self.update()
        w = self.canvas.winfo_width() - 10  # substract scroll width
        self.justification_label.config(wraplength=w)

    def get_just_text(self):
        if not self.more_detail:
            if self.language_button_labels[self.language_button_state] == 'Manchester OWL':
                return self.human_just_texts[0]
            elif self.language_button_labels[self.language_button_state] == 'Natural Language':
                return self.manchester_just_texts[0]

        else:
            t = ''
            if self.language_button_labels[self.language_button_state] == 'Manchester OWL':
                for text in self.human_just_texts:
                    t += text + '\n\n'
            elif self.language_button_labels[self.language_button_state] == 'Natural Language':
                for text in self.manchester_just_texts:
                    t += text + '\n\n'
            return t

if __name__ == '__main__':
    app = JustificationPlatform()
    app.mainloop()
