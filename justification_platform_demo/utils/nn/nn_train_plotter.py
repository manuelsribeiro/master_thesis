# import the necessary packages
import matplotlib.pyplot as plt


class TrainPlotter:
    def __init__(self, hist, show=False, output_filename=None):
        self.hist = hist
        self.show = show
        self.output_filename = output_filename

    def plot_train_validation(self):
        plt.figure()
        plt.plot(self.hist.history["loss"], label="train_loss")
        plt.plot(self.hist.history["val_loss"], label="val_loss")
        plt.plot(self.hist.history["accuracy"], label="train_acc")
        plt.plot(self.hist.history["val_accuracy"], label="val_acc")
        plt.title("Training Loss and Accuracy")
        plt.xlabel("Epoch #")
        plt.ylabel("Loss/Accuracy")
        plt.margins(x=0)
        plt.margins(y=0)
        plt.legend()

        if self.show:
            plt.show()

        if self.output_filename is not None:
            plt.savefig(self.output_filename)

        plt.close()
