class ShallowNet:
    @staticmethod
    def build(width, height, depth, classes):
        from tensorflow.keras import Sequential
        from tensorflow.keras.layers import Activation, Conv2D, Dense, Flatten
        from tensorflow.keras import backend as K

        # initialize the model along with the input shape to be
        # "channels last"
        model = Sequential()
        input_shape = (height, width, depth)

        # if we are using "channels first", update the input shape
        if K.image_data_format() == "channels_first":
            input_shape = (depth, height, width)

        # define the first (and only) CONV => RELU layer
        model.add(Conv2D(32, (3, 3), padding="same", input_shape=input_shape))
        model.add(Activation("relu"))

        # softmax classifier
        model.add(Flatten())
        model.add(Dense(classes))
        if classes > 1:
            model.add(Activation("softmax"))
        else:
            model.add(Activation("sigmoid"))

        # return the constructed network architecture
        return model
