class LeNet:
    @staticmethod
    def build(width, height, depth, classes):
        from tensorflow.keras import Sequential
        from tensorflow.keras.layers import Activation, Conv2D, Dense, Flatten, MaxPool2D
        from tensorflow.keras import backend as K

        # initialize the model
        model = Sequential()
        input_shape = (height, width, depth)

        # if we are using "channels first", update the input shape
        if K.image_data_format() == "channels_first":
            input_shape = (depth, height, width)

        # first set of CONV => RELU => POOL layers
        model.add(Conv2D(20, (5, 5), padding="same", input_shape=input_shape))
        model.add(Activation("relu"))
        model.add(MaxPool2D(pool_size=(2, 2), strides=(2, 2)))

        # second set of CONV => REU => POOL layers
        model.add(Conv2D(50, (5, 5), padding="same"))
        model.add(Activation("relu"))
        model.add(MaxPool2D(pool_size=(2, 2), strides=(2, 2)))

        # first (and only) set of FC => RELU layers
        model.add(Flatten())
        model.add(Dense(500))
        model.add(Activation("relu"))

        # softmax classifier
        model.add(Dense(classes))
        if classes > 1:
            model.add(Activation("softmax"))
        else:
            model.add(Activation("sigmoid"))

        # return the constructed network architecture
        return model
