import collections
import gc
import math
import numpy as np
import os
import pandas as pd
import PIL
import sys

from utils.activation_extractor.activation_extractor import ActivationExtractor


def _select_samples(data, balance_class, samples_by_value):
    chosen_indices = []

    # if needed pick more samples of a given value
    for value in samples_by_value:
        missing_samples = samples_by_value[value]
        if missing_samples != 0:
            indices = data[data[balance_class] == value].index
            indices = indices[:missing_samples]
            chosen_indices.extend(indices)
            samples_by_value[value] -= len(indices)

    return chosen_indices, samples_by_value


def _check_missing_samples(dataframe, num_samples, balance_class, missing_samples):
    if balance_class is None:
        if len(dataframe) != num_samples:
            print("[Error] Abort, not enough samples in dataset.", file=sys.stderr)
            exit(-1)

    else:
        if len(set(missing_samples.values())) != 1 or 0 not in set(missing_samples.values()):
            print("[Error] Abort, not enough samples in dataset.", file=sys.stderr)
            for value in missing_samples:
                if missing_samples[value] != 0:
                    print("[Error] Lacking {} samples with `{}' value {}.".format(
                        missing_samples[value], balance_class, value), file=sys.stderr)
            exit(-1)


def prepare_dataset_csv(path_to_dataset, num_samples=None,
                        balance_class=None, samples_by_value=None, cols=None, shuffle=False, ordered=False):
    """ Reads data from a CSV - rows for samples and columns for features - and returns a pandas DataFrame with
    num_samples rows.

    Parameters
    ----------
    path_to_dataset : str
        Path to CSV file to be read
    num_samples : int, optional
        Number of samples (rows) to put in the returned dataframe
    balance_class : str, optional
        Class used to balance the dataset, to be used together with `samples_by_value' parameter
    samples_by_value : dict of {Any, int}, optional
        Amount of samples for each value of the range of the balance_class, to be used together with `balance_class'
         parameter
    cols : list of str, optional
        List of features to be put into the dataset
    shuffle : bool, optional
        Whether to shuffle the dataset before returning it
    ordered : bool, optional
        If true selects the samples in the same order as they appear in the CSV file - `path_to_dataset'
         parameter, else randomly select samples from the CSV file.

    Returns
    -------
    pandas.DataFrame
        Pandas DataFrame with the selected features and samples from the CSV file.
    """
    if balance_class is not None and samples_by_value is None:
        print("[Error] Abort, a balancing class was set, but not the amount of samples for each of its range values.",
              file=sys.stderr)
        print("[Error] Please specify the amount of samples for each range value using the parameter"
              " `samples_by_class_value'.", file=sys.stderr)
        exit(-1)

    if balance_class is None and samples_by_value is not None:
        print("[Error] Abort, an amount of samples for a set of range values was defined, but no balancing class was"
              " specified.",
              file=sys.stderr)
        print("[Error] Please specify the balancing class using the parameter `balance_class'.", file=sys.stderr)
        exit(-1)

    if samples_by_value is not None and num_samples != np.sum(list(samples_by_value.values())):
        print("[Error] Abort, the amount of samples to be put in the dataset - `num_samples' - does not match with the"
              " amount of samples specified in the parameter `samples_by_value'.", file=sys.stderr)
        exit(-1)

    if balance_class is None:
        if ordered:  # simply take the first `num_samples' samples
            res = pd.read_csv(path_to_dataset, usecols=cols, nrows=num_samples, na_values=['NA', '?'])
        else:  # shuffle the dataset and take the first `num_samples' samples
            res = pd.read_csv(path_to_dataset, usecols=cols, na_values=['NA', '?'])
            res = res.reindex(np.random.permutation(res.index))
            res = res.head(n=num_samples)

    else:  # it is necessary to balance the dataset
        samples_by_value = samples_by_value.copy()  # to keep it unchanged outside this function

        # if necessary add balance_class feature to the columns to be read
        cols_to_read = cols
        if cols is not None and balance_class not in cols:
            cols_to_read = set(cols)
            cols_to_read.add(balance_class)

        res = pd.read_csv(path_to_dataset, usecols=cols_to_read, nrows=0, na_values=['NA', '?'])

        if ordered:
            for chunk in pd.read_csv(path_to_dataset,
                                     usecols=cols_to_read, na_values=['NA', '?'], chunksize=2*num_samples):
                chosen_indices, missing_samples = _select_samples(chunk, balance_class, samples_by_value)

                res = res.append(chunk.loc[chosen_indices])
                samples_by_value = missing_samples

                # all needed samples loaded - all missing samples have a value of 0
                missing_samples = set(missing_samples.values())
                if len(missing_samples) == 1 and 0 in missing_samples:
                    break
        else:
            df = pd.read_csv(path_to_dataset, usecols=cols_to_read, na_values=['NA', '?'])
            df = df.reindex(np.random.permutation(df.index))

            for chunk in np.array_split(df, len(df)//(2*num_samples)):
                chosen_indices, missing_samples = _select_samples(chunk, balance_class, samples_by_value)

                res = res.append(chunk.loc[chosen_indices])
                samples_by_value = missing_samples

                # all needed samples loaded - all missing samples have a value of 0
                missing_samples = set(missing_samples.values())
                if len(missing_samples) == 1 and 0 in missing_samples:
                    break

    # check if the given values were plausible with the given dataset
    if num_samples is not None:
        _check_missing_samples(res, num_samples, balance_class, samples_by_value)

    # if necessary remove balance_class feature from the read dataset
    if cols is not None and balance_class is not None and balance_class not in cols:
        res.drop(balance_class, axis=1, inplace=True)

    res.reset_index(drop=True, inplace=True)

    if shuffle:
        # shuffle the new dataset and return it
        return res.reindex(np.random.permutation(res.index))
    else:
        return res.sort_index()


def _prepare_images(images):
    # scale the raw pixel intensities to the range [-1, 1]
    images = images / 127.5
    images = images - 1.0
    images.astype(np.float32)
    return images


def load_gtsrb_images(images_to_load, path_to_images, preprocess=True):
    """
    Loads all images in the given list of trains and returns them in an numpy ndarray.

    Parameters
    ----------
    images_to_load : list of int
        List of trains to load
    path_to_images : str
        Path to the folder containing the .npz files containing the images
    preprocess : bool, optional
        Whether to preprocess the images before returning them

    Returns
    -------
    ndarray
        Numpy ndarray containing all of the loaded images.
    """

    data = []

    for img_file in images_to_load:
        img = PIL.Image.open(path_to_images + img_file)
        img = img.resize((128, 128), resample=PIL.Image.NEAREST)
        data.append(np.array(img))

    # concatenate all block images
    data = np.stack(data, axis=0)

    if preprocess:
        data = _prepare_images(data)

    return data


def extract_activations_dataset(model, layers_to_extract, path_to_csv, path_to_images, num_samples=None,
                                balance_class=None, samples_by_value=None, cols=None, shuffle=True, ordered=False):
    """ Extract activations from a set of layers of a neural network model for a given set of samples and return a
     dataset containing those activations.

    Parameters
    ----------
    model : Keras Sequential NeuralNetwork
        Neural network from which the activations are extracted.
    layers_to_extract : list of str
        Layers which activations will be extracted
    path_to_csv : str
        Path to CSV file to be read
    path_to_images : str
        Path to the folder containing the .npz files containing the images
    num_samples : int, optional
        Amount of samples to load
    balance_class : str, optional
        Class used to balance the dataset, to be used together with `samples_by_value' parameter
    samples_by_value : dict of {Any, int}, optional
        Amount of samples for each value of the range of the balance_class, to be used together with `balance_class'
    cols : list of str, optional
        List of features to be put into the dataset
    shuffle : bool, optional
        Whether to shuffle the dataset before returning it
    ordered : bool, optional
        If true selects the samples in the same order as they appear in the CSV file - `path_to_dataset'
         parameter, else randomly select samples from the CSV file.
    Returns
    -------
        pandas.DataFrame
        Pandas DataFrame with the activations from the neural network for each selected sample.
    """

    # we select which examples we wish to use
    selected_samples = prepare_dataset_csv(path_to_csv, num_samples=num_samples, balance_class=balance_class,
                                           samples_by_value=samples_by_value, cols=cols, shuffle=shuffle,
                                           ordered=ordered)

    # load selected samples
    #image_files = [filename.replace('_', '/', 1) for filename in selected_samples['Filename'].values]
    image_files = selected_samples['Filename'].values
    data = load_gtsrb_images(image_files, path_to_images)  # rows in data follow the same order as in selected_samples

    # extract activations
    act_extractor = ActivationExtractor(model, layers_to_extract)
    act_df = act_extractor.extract_activations(data, pandas=True)  # rows in act_df follow the same order as in data
    act_df.set_index(keys=selected_samples.index, inplace=True)  # set index of act_df to be that of selected_samples

    #act_df = pd.merge(selected_samples, act_df, left_index=True, right_index=True)   # this is wrong
    act_df = pd.concat([selected_samples, act_df], axis=1)

    if shuffle:
        return act_df.reindex(np.random.permutation(act_df.index))
    else:
        return act_df
