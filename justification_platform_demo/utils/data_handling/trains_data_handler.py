import collections
import gc
import math
import numpy as np
import os
import pandas as pd
import sys

from utils.activation_extractor import ActivationExtractor


def _select_samples(data, balance_class, samples_by_value):
    chosen_indices = []

    # if needed pick more samples of a given value
    for value in samples_by_value:
        missing_samples = samples_by_value[value]
        if missing_samples != 0:
            indices = data[data[balance_class] == value].index
            indices = indices[:missing_samples]
            chosen_indices.extend(indices)
            samples_by_value[value] -= len(indices)

    return chosen_indices, samples_by_value


def _check_missing_samples(dataframe, num_samples, balance_class, missing_samples):
    if balance_class is None:
        if len(dataframe) != num_samples:
            print("[Error] Abort, not enough samples in dataset.", file=sys.stderr)
            exit(-1)

    else:
        if len(set(missing_samples.values())) != 1 or 0 not in set(missing_samples.values()):
            print("[Error] Abort, not enough samples in dataset.", file=sys.stderr)
            for value in missing_samples:
                if missing_samples[value] != 0:
                    print("[Error] Lacking {} samples with `{}' value {}.".format(
                        missing_samples[value], balance_class, value), file=sys.stderr)
            exit(-1)


def prepare_dataset_csv(path_to_dataset, num_samples,
                        balance_class=None, samples_by_value=None, cols=None, shuffle=False, ordered=False):
    """ Reads data from a CSV - rows for samples and columns for features - and returns a pandas DataFrame with
    num_samples rows.

    Parameters
    ----------
    path_to_dataset : str
        Path to CSV file to be read
    num_samples : int
        Number of samples (rows) to put in the returned dataframe
    balance_class : str, optional
        Class used to balance the dataset, to be used together with `samples_by_value' parameter
    samples_by_value : dict of {Any, int}, optional
        Amount of samples for each value of the range of the balance_class, to be used together with `balance_class'
         parameter
    cols : list of str, optional
        List of features to be put into the dataset
    shuffle : bool, optional
        Whether to shuffle the dataset before returning it
    ordered : bool, optional
        If true selects the samples in the same order as they appear in the CSV file - `path_to_dataset'
         parameter, else randomly select samples from the CSV file.

    Returns
    -------
    pandas.DataFrame
        Pandas DataFrame with the selected features and samples from the CSV file.
    """
    if balance_class is not None and samples_by_value is None:
        print("[Error] Abort, a balancing class was set, but not the amount of samples for each of its range values.",
              file=sys.stderr)
        print("[Error] Please specify the amount of samples for each range value using the parameter"
              " `samples_by_class_value'.", file=sys.stderr)
        exit(-1)

    if balance_class is None and samples_by_value is not None:
        print("[Error] Abort, an amount of samples for a set of range values was defined, but no balancing class was"
              " specified.",
              file=sys.stderr)
        print("[Error] Please specify the balancing class using the parameter `balance_class'.", file=sys.stderr)
        exit(-1)

    if samples_by_value is not None and num_samples != np.sum(list(samples_by_value.values())):
        print("[Error] Abort, the amount of samples to be put in the dataset - `num_samples' - does not match with the"
              " amount of samples specified in the parameter `samples_by_value'.", file=sys.stderr)
        exit(-1)

    if balance_class is None:
        if ordered:  # simply take the first `num_samples' samples
            res = pd.read_csv(path_to_dataset, usecols=cols, nrows=num_samples, na_values=['NA', '?'])
        else:  # shuffle the dataset and take the first `num_samples' samples
            res = pd.read_csv(path_to_dataset, usecols=cols, na_values=['NA', '?'])
            res = res.reindex(np.random.permutation(res.index))
            res = res.head(n=num_samples)

    else:  # it is necessary to balance the dataset
        samples_by_value = samples_by_value.copy()  # to keep it unchanged outside this function

        # if necessary add balance_class feature to the columns to be read
        cols_to_read = cols
        if cols is not None and balance_class not in cols:
            cols_to_read = set(cols)
            cols_to_read.add(balance_class)

        res = pd.read_csv(path_to_dataset, usecols=cols_to_read, nrows=0, na_values=['NA', '?'])

        if ordered:
            for chunk in pd.read_csv(path_to_dataset,
                                     usecols=cols_to_read, na_values=['NA', '?'], chunksize=2*num_samples):
                chosen_indices, missing_samples = _select_samples(chunk, balance_class, samples_by_value)

                res = res.append(chunk.loc[chosen_indices])
                samples_by_value = missing_samples

                # all needed samples loaded - all missing samples have a value of 0
                missing_samples = set(missing_samples.values())
                if len(missing_samples) == 1 and 0 in missing_samples:
                    break
        else:
            df = pd.read_csv(path_to_dataset, usecols=cols_to_read, na_values=['NA', '?'])
            df = df.reindex(np.random.permutation(df.index))

            for chunk in np.array_split(df, len(df)//(2*num_samples)):
                chosen_indices, missing_samples = _select_samples(chunk, balance_class, samples_by_value)

                res = res.append(chunk.loc[chosen_indices])
                samples_by_value = missing_samples

                # all needed samples loaded - all missing samples have a value of 0
                missing_samples = set(missing_samples.values())
                if len(missing_samples) == 1 and 0 in missing_samples:
                    break

    # check if the given values were plausible with the given dataset
    _check_missing_samples(res, num_samples, balance_class, samples_by_value)

    # if necessary remove balance_class feature from the read dataset
    if cols is not None and balance_class is not None and balance_class not in cols:
        res.drop(balance_class, axis=1, inplace=True)

    if shuffle:
        # shuffle the new dataset and return it
        return res.reindex(np.random.permutation(res.index))
    else:
        return res.sort_index()


def prepare_dataset_pickles(path_to_dataset, selected_samples, shuffle=True, samples_per_pkl=5_000):
    # load only the necessary blocks of samples (.pkl files)
    files = set(selected_samples // samples_per_pkl)

    res = []

    for file_num in files:
        block_name = path_to_dataset + 'activations_' + str(file_num) + '.pkl'
        sample_block = pd.read_pickle(block_name)
        res.append(sample_block.loc[sample_block['name'].isin(selected_samples)])

    res = pd.concat(res, axis=0)
    res.reset_index(drop=True, inplace=True)

    if shuffle:
        # shuffle the new dataset and return it
        return res.reindex(np.random.permutation(res.index))
    else:
        return res


def _prepare_images(images):
    # scale the raw pixel intensities to the range [0, 1]
    images = images / 255.0
    images.astype(np.float32)
    return images


def load_trains_images(trains_to_load, path_to_images, ordered=True, images_per_npz=5_000):
    """
    Loads all images in the given list of trains and returns them in an numpy ndarray.

    Parameters
    ----------
    trains_to_load : list of int
        List of trains to load
    path_to_images : str
        Path to the folder containing the .npz files containing the images
    ordered : bool, optional
        Whether to return the images in the same order as they appear in the list of trains to load - `trains_to_load'
         parameter
    images_per_npz : int, optional
        Amount of images in each .npz file containing images

    Returns
    -------
    ndarray
        Numpy ndarray containing all of the loaded images.
    """

    data = []
    # load only the necessary blocks of images (.npz files)
    files = list(set(trains_to_load // images_per_npz))
    files.sort()

    images_by_file = collections.defaultdict(list)
    for image in trains_to_load:
        images_by_file[image // images_per_npz].append(image)

    if ordered:
        img_dict = {}
        for file_num in files:
            block_name = path_to_images + str(file_num).zfill(3) + ".npz"
            block_images = np.load(block_name)['images']
            for image in images_by_file[file_num]:
                # if image belongs to block, get it and store it in data array
                img_dict[image] = block_images[image % images_per_npz].copy()

        for img in trains_to_load:
            data.append(img_dict[img])

    else:
        for file_num in files:
            block_name = path_to_images + str(file_num).zfill(3) + ".npz"
            block_images = np.load(block_name)['images']
            for image in images_by_file[file_num]:
                # if image belongs to block, get it and store it in data array
                data.append(block_images[image % images_per_npz].copy())

    # concatenate all block images
    data = np.stack(data, axis=0)

    data = _prepare_images(data)

    return data


def save_nn_activations(path_to_model, layers, dataframe, path_to_images, path_to_activations, images_per_pickle=5_000):
    """ Stores in a pickle file a pandas DataFrame with the activations extracted from the specified layers in `layers'
    parameter.

    Parameters
    ----------
    path_to_model : str
        Path to neural network model from which the activations are to be extracted
    layers : list of str
        Layers which activations will be extracted
    dataframe : pandas.Dataframe
        Dataframe containing the samples which activations should be saved
    path_to_images : str
        Path to the folder containing the .npz files containing the images
    path_to_activations : str
        Path to where the activations dataset are saved
    images_per_pickle : int, optional
        Amount of images in each .pkl file containing images

    Returns
    -------
    pandas.DataFrame
        Pandas DataFrame with the activations from the neural network for each selected sample.
    """
    from tensorflow.keras.models import load_model

    # load the model to analyze
    model = load_model(path_to_model)

    for layer in layers:
        dataset_path = os.path.join(path_to_activations, layer)

        if not os.path.exists(dataset_path):  # first time extracting this layer activations from this network
            # create appropriate folder
            os.makedirs(dataset_path)

            dataset_filename = os.path.join(dataset_path, 'activations_')
            act_extractor = ActivationExtractor(model, [layer])

            # divide in multiple pickles
            for chunk_num in range(math.ceil(len(dataframe) / images_per_pickle)):
                temp_df = dataframe[chunk_num * images_per_pickle: (chunk_num + 1) * images_per_pickle]
                data = load_trains_images(temp_df['name'].values, path_to_images)

                act_df = act_extractor.extract_activations(data, pandas=True)

                act_df.insert(0, 'name', temp_df['name'].values)
                act_df.to_pickle(dataset_filename + "{}.pkl".format(chunk_num))

                gc.collect()


def load_activations_dataset(layers, dataset_size, balance_class, samples_by_value, path_to_csv,
                             path_to_activations, shuffle=True):
    """ Loads the activations from a given set of layers from a neural network from a stored dataset.

    Parameters
    ----------
    layers : list of str
        Layers which activations will be loaded
    dataset_size : int
        Amount of samples to load
    balance_class : str
        Class used to balance the dataset, to be used together with `samples_by_value' parameter
    samples_by_value : dict of {Any, int}, optional
        Amount of samples for each value of the range of the balance_class, to be used together with `balance_class'
         parameter
    path_to_csv : str
        Path to CSV file to be read
    path_to_activations : str
        Path to where the activations dataset are saved
    shuffle : bool, optional
        Whether to shuffle the dataset before returning it

    Returns
    -------
        pandas.DataFrame
        Pandas DataFrame with the activations from the neural network for each selected sample.
    """

    # we select which examples we wish to use
    selected_samples = prepare_dataset_csv(path_to_csv, dataset_size, balance_class,
                                           samples_by_value=samples_by_value, cols=['name', balance_class])
    selected_samples.sort_index(inplace=True)  # sort so that all dataframes are in the same order
    selected_samples.reset_index(drop=True, inplace=True)  # reset index so that all dataframes have the same index

    # join all of the layer activations datasets
    df = [selected_samples]

    for layer in layers:
        print("[INFO] Loading layer {} activations...".format(layer))
        dataset_path = path_to_activations + '/' + layer + '/'
        # layer_act_df is already sorted if we dont shuffle
        layer_act_df = prepare_dataset_pickles(dataset_path, selected_samples['name'].values, shuffle=False)
        layer_act_df.drop('name', axis=1, inplace=True)  # drop name column, there is no need for it
        layer_act_df.reset_index(drop=True, inplace=True)  # reset index so that all dataframes have the same index
        df.append(layer_act_df)

    df = pd.concat(df, axis=1)

    if shuffle:
        return df.reindex(np.random.permutation(df.index))
    else:
        return df


def extract_activations_dataset(model, layers_to_extract, dataset_size, balance_class, samples_by_value, path_to_csv,
                                path_to_images, shuffle=True):
    """ Extract activations from a set of layers of a neural network model for a given set of samples and return a
     dataset containing those activations.

    Parameters
    ----------
    model : Keras Sequential NeuralNetwork
        Neural network from which the activations are extracted.
    layers_to_extract : list of str
        Layers which activations will be extracted
    dataset_size : int
        Amount of samples to load
    balance_class : str
        Class used to balance the dataset, to be used together with `samples_by_value' parameter
    samples_by_value : dict of {Any, int}, optional
        Amount of samples for each value of the range of the balance_class, to be used together with `balance_class'
    path_to_csv : str
        Path to CSV file to be read
    path_to_images : str
        Path to the folder containing the .npz files containing the images
    shuffle : bool
        Whether to shuffle the dataset before returning it
    Returns
    -------
        pandas.DataFrame
        Pandas DataFrame with the activations from the neural network for each selected sample.
    """
    # we select which examples we wish to use
    selected_samples = prepare_dataset_csv(path_to_csv, dataset_size, balance_class=balance_class,
                                           samples_by_value=samples_by_value, cols=['name', balance_class])
    selected_samples.sort_index(inplace=True)  # sort so that all dataframes are in the same order
    selected_samples.reset_index(drop=True, inplace=True)  # reset index so that all dataframes have the same index

    # load selected samples
    data = load_trains_images(selected_samples['name'].values, path_to_images)

    # extract activations
    act_extractor = ActivationExtractor(model, layers_to_extract)
    act_df = act_extractor.extract_activations(data, pandas=True)
    act_df = pd.concat([selected_samples, act_df], axis=1)

    if shuffle:
        return act_df.reindex(np.random.permutation(act_df.index))
    else:
        return act_df
