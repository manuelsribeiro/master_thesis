from tensorflow.keras.models import Model
from typing import List
import pandas as pd
import numpy as np


class ActivationExtractor:
    """Class representing an object capable of extracting the activations of a set of layers from a neural network."""

    def __init__(self, model, layers: List[str]):
        """
        model - a trained neural network, assumed to be of class tensorflow.python.keras.engine.sequential.Sequential
        layers - sequence of layers to extract
        """
        self.model = model
        self.layers = layers
        self._outs = [model.get_layer(layer_name).output for layer_name in layers]
        self.extractor = Model(inputs=model.input, outputs=self._outs)

    def extract_activations(self, data, batch_size=32, pandas=False):
        """
        Given a set of samples, returns a numpy array/pandas dataframe of the layers activations.
        :return: a numpy array/pandas dataframe of the layers activations
        """
        if pandas:
            return self._extract_activations_pandas(data, batch_size)
        else:
            return self._extract_activations_numpy(data, batch_size)

    def _get_activations_by_layer(self, data, batch_size=32):
        # get the activations - in shape (#layers, #samples, (#neurons_of_layer, ...))
        activations = self.extractor.predict(data, batch_size=batch_size)
        if len(self.layers) == 1:
            activations = np.expand_dims(activations, axis=0)
        # shape (#layers, #samples, #neurons_of_layer)
        activations = [layer_act.reshape((layer_act.shape[0], np.prod(layer_act.shape[1:], dtype=np.int)))
                       for layer_act in activations]
        return activations

    def _extract_activations_numpy(self, data, batch_size=32):
        """
        Given a set of samples, returns a numpy array of the layers activations.
        :return: a numpy array of the layers activations with shape (#samples, #neurons)
        """
        # get the activations - in shape (#layers, #samples, #neurons_of_layer)
        activations = self._get_activations_by_layer(data, batch_size)

        # shape (#samples, #neurons)
        activations = np.concatenate(activations, axis=1)

        return activations

    def _extract_activations_pandas(self, data, batch_size=32):
        """
        Given a set of samples, returns a (pandas) dataframe of the layers activations.
        :return: pandas dataframe of the layers activations
        """
        # get the activations - in shape (#layers, #samples, #neurons_of_layer)
        activations = self._get_activations_by_layer(data, batch_size)

        neuron_names = [layer + "_{}".format(neuron_num)
                        for layer_num, layer in enumerate(self.layers)
                        for neuron_num in range(activations[layer_num].shape[1])]

        # shape (#samples, #neurons)
        activations = np.concatenate(activations, axis=1)

        return pd.DataFrame(activations, columns=neuron_names)
