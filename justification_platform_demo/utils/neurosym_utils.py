from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split

import numpy as np


def tf_set_memory_growth():
    import tensorflow as tf

    gpus = tf.config.experimental.list_physical_devices('GPU')

    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
            logical_gpus = tf.config.experimental.list_logical_devices('GPU')
            print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)


def train_val_test_split(data, labels, train_size, val_size, test_size, random_seed, shuffle):
    (X_remain, X_test, Y_remain, Y_test) = train_test_split(
        data, labels, train_size=train_size+val_size, test_size=test_size,
        random_state=random_seed, shuffle=shuffle, stratify=labels)

    (X_train, X_val, Y_train, Y_val) = train_test_split(
        X_remain, Y_remain, train_size=train_size, test_size=val_size,
        random_state=random_seed, shuffle=shuffle, stratify=Y_remain)

    return X_train, X_val, X_test, Y_train, Y_val, Y_test


def train_model(model, x_train, y_train, x_val, y_val, max_epochs, patience, batch_size, weights_name, seed=None):
    import tensorflow as tf
    from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
    from tensorflow.keras.models import load_model
    if seed is not None:
        tf.random.set_seed(seed)

    # early stopping
    monitor = EarlyStopping(monitor='val_loss', min_delta=1e-4, patience=patience, verbose=1, mode='auto')

    # construct the callback to save only the *best* model to based on the validation loss
    checkpoint = ModelCheckpoint(weights_name, monitor='val_loss', mode='min', save_best_only=True, verbose=1)

    # fit the model to the training set
    hist = model.fit(x_train, y_train, validation_data=(x_val, y_val), callbacks=[checkpoint, monitor],
                     verbose=0, epochs=max_epochs, batch_size=batch_size, shuffle=True)

    # load the best trained network
    model = load_model(weights_name)

    return model, hist


def evaluate_model(model, x_train, y_train, x_val, y_val, x_test, y_test):
    pred = model.predict(x_train)
    tr_score = accuracy_score(y_train, np.rint(pred))
    pred = model.predict(x_val)
    val_score = accuracy_score(y_val, np.rint(pred))
    pred = model.predict(x_test)
    test_score = accuracy_score(y_test, np.rint(pred))
    return tr_score, val_score, test_score
