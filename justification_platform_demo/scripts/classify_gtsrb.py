from tensorflow.keras.models import load_model

import argparse
import numpy as np
import pandas as pd
import PIL

"""
Note: for reproducibility set the environment variable PYTHONHASHSEED to value 0, and CUDA_VISIBLE_DEVICES to value "".
"""

CONCEPTS_MAP = {
    'A': 'having a traffic signal of category A',
    'B': 'having a traffic signal of category B',
    'C': 'having a traffic signal of category C',
    'D': 'having a traffic signal of category C',
}

# program arguments and constants, to be changed depending on the experiment
parser = argparse.ArgumentParser()
parser.add_argument('-nn', '--nn', help='path to neural network model (.hdf5)', type=str, required=True)
parser.add_argument('-i', '--input', help='path to neural network\'s input (.png)', type=str, required=True)
parser.add_argument('-c', '--out_class', help='output class to justify', type=str, required=True)

args = parser.parse_args()

PATH_TO_ORIGINAL_NN = args.nn
TARGET_IMAGE = args.input
OUTPUT_CLASS = args.out_class

OUT_FILE = './temp/observations/classification.txt'
PATH_TO_CSV = './datasets/gtsrb_subset/GT-final_test.csv'

ORIGINAL_CONCEPT = "SignClass"

def get_concept_num(concept):
    if concept == 'A':
        return 0
    elif concept == 'B':
        return 1
    elif concept == 'C':
        return 2
    elif concept == 'D':
        return 3


def main():
    print("[INFO] Loading input image...")
    image_num = TARGET_IMAGE.split('/')[-1].split('\\')[-1]
    original_img = PIL.Image.open(TARGET_IMAGE)
    original_img = original_img.resize((128, 128), resample=PIL.Image.NEAREST)
    original_img = np.array(original_img)

    preprocessed_img = (original_img / 127.5) - 1.0
    preprocessed_img.astype(np.float32)
    preprocessed_img = np.asarray([preprocessed_img])  # dtype = np.float32

    print("[INFO] Loading original network...")
    model = load_model(PATH_TO_ORIGINAL_NN)
    original_res = model.predict(preprocessed_img)
    original_res = original_res[0]
    #original_res = np.round(original_res)
    arg_max = np.argmax(original_res)
    #print(original_res); print(arg_max);exit(-1)
    if arg_max == 0:   res = 'A'
    elif arg_max == 1: res = 'B'
    elif arg_max == 2: res = 'C'
    elif arg_max == 3: res = 'D'

    label = pd.read_csv(PATH_TO_CSV, usecols=['Filename', ORIGINAL_CONCEPT], na_values=['NA', '?'])
    label = label.loc[label['Filename'] == image_num, [ORIGINAL_CONCEPT]]
    label = label.values[0][0]

    if res == label:
        guessed = 'correctly'
    else:
        guessed = 'incorrectly'

    if original_res[get_concept_num(OUTPUT_CLASS)] <= 0.5:
        classification = 'not ' + CONCEPTS_MAP[OUTPUT_CLASS]
    else:
        classification = CONCEPTS_MAP[OUTPUT_CLASS]

    with open(OUT_FILE, 'w') as classification_file:
        print('The original network {} classified the input as an image {}.'.format(
            guessed, classification))
        classification_file.write('The original network {} classified the input as an image {}.'.format(
            guessed, classification))

        classification_file.close()

    print("[INFO] Finished")


if __name__ == '__main__':
    main()
