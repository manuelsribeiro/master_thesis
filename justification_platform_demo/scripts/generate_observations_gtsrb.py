from tensorflow.keras.models import load_model

import argparse
import numpy as np
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from utils.activation_extractor import ActivationExtractor

"""

"""

PREFIX = '__input__ Type: '
NEGATION = 'not '

CONCEPTS_MAP = {  # translate from dataset's column headers to ontology's concepts
    'A': '(A)',
    'B': '(B)',
    'C': '(C)',
    'D': '(D)',
    'Circular_Shape': '(hasShape some Circle)',
    'Red_Ground': '(hasGround some Red)',
    'Symbol': '(hasSymbol some)',
    'White_Bar': '(hasBar some White)',
    'White_Ground': '(hasGround some White)',
    'Red_Border': '(hasBorder some Red)',
    'Start_Prohibition': '(StartProhibition)',
    'Symbol_NoEntryGoods': '(hasSymbol some NoEntryGoods)',
    'Bar': '(hasBar some)',
    'Symbol_Overtaking': '(hasSymbol some Overtaking)',
    'Symbol_OvertakingGoods': '(hasSymbol some OvertakingGoods)',
    'Symbol_Speed20': '(hasSymbol some Speed20)',
    'Symbol_Speed30': '(hasSymbol some Speed30)',
    'Symbol_Speed50': '(hasSymbol some Speed50)',
    'Symbol_Speed60': '(hasSymbol some Speed60)',
    'Symbol_Speed70': '(hasSymbol some Speed70)',
    'Symbol_Speed80': '(hasSymbol some Speed80)',
    'Symbol_Speed100': '(hasSymbol some Speed100)',
    'Symbol_Speed120': '(hasSymbol some Speed120)',
    'Border': '(hasBorder some)',
    'Black_Border': '(hasBorder some Black)',
    'Black_Bar': '(hasBar some Black)',
    'Triangular_Shape': '(hasShape some Triangle)',
    'Octagonal_Shape': '(hasShape some Octagon)',
    'White_Symbol': '(hasSymbol some White)',
    'Symbol_Stop': '(hasSymbol some Stop)',
    'Diamond_Shape': '(hasShape some Diamond)',
    'Yellow_Ground': '(hasGround some Yellow)',
    'White_Border': '(hasBorder some White)',
    'D1a1': 'D1a1',
    'D1a4': 'D1a4',
    'D1a5': 'D1a5',
    'D1a6': 'D1a6',
    'D1a7': 'D1a7',
    'D2a1': 'D2a1',
    'D2a2': 'D2a2',
    'D3': 'D3',
    'Black_Symbol': '(hasSymbol some Black)',
}

# program arguments and constants, to be changed depending on the experiment
parser = argparse.ArgumentParser()
parser.add_argument('-nn', '--nn', help='path to neural network model (.hdf5)', type=str, required=True)
parser.add_argument('-i', '--input', help='path to neural network\'s input (.png)', type=str, required=True)
parser.add_argument('-c', '--concepts', nargs='+', help='concepts to map', type=str, required=True)

args = parser.parse_args()

PATH_TO_ORIGINAL_NN = args.nn
TARGET_IMAGE = args.input
MAPPING_CONCEPTS = args.concepts

LAYERS_TO_EXTRACT = [
    'block_1_project_BN',
    'block_2_project_BN',
    'block_3_project_BN',
    'block_4_project_BN',
    'block_5_project_BN',
    'block_6_project_BN',
    'block_7_project_BN',
    'block_8_project_BN',
    'block_9_project_BN',
    'block_10_project_BN',
    'block_11_project_BN',
    'block_12_project_BN',
    'block_13_project_BN',
    'block_14_project_BN',
    'block_15_project_BN',
    'block_16_project_BN'
]

PATH_TO_WEIGHTS = './temp/mapping_nns/'


def _to_original_observation(concept, prefix):
    return prefix + concept + '\n'


def _to_mapping_observation(concept, prefix, confidence):
    if confidence >= 0.5:
        return prefix + concept + ", " + str(confidence) + '\n'
    else:
        return prefix + _not(concept) + ", " + str((1-confidence)) + '\n'


def _not(text):
    return NEGATION + text


print("[INFO] Loading input image...")
import PIL
original_img = PIL.Image.open(TARGET_IMAGE)
original_img = original_img.resize((128, 128), resample=PIL.Image.NEAREST)
original_img = np.array(original_img)

preprocessed_img = (original_img / 127.5) - 1.0
preprocessed_img.astype(np.float32)
preprocessed_img = np.asarray([preprocessed_img])  # dtype = np.float32

print("[INFO] Loading original network...")
model = load_model(PATH_TO_ORIGINAL_NN)
print("[INFO] Loading mapping networks...")
mapping_nns = []
for concept in MAPPING_CONCEPTS:
    path_to_nn = PATH_TO_WEIGHTS + concept + '.hdf5'
    mapping_nns.append(load_model(path_to_nn))

# predict the original image output
original_res = model.predict(preprocessed_img)
original_res = original_res[0]
arg_max = np.argmax(original_res)

if arg_max == 0:
    main_res = 'A'
elif arg_max == 1:
    main_res = 'B'
elif arg_max == 2:
    main_res = 'C'
elif arg_max == 3:
    main_res = 'D'
print("[INFO] Main model predicts: {}".format(main_res))

# extract activations from the occluded dataset for the chosen layers
extractor = ActivationExtractor(model.layers[1], LAYERS_TO_EXTRACT)
activations_original = extractor.extract_activations(preprocessed_img)

del model

concepts_res = []
for i, nn in enumerate(mapping_nns):
    # predict the original image output
    res = nn.predict(activations_original)
    res = res[0][0]
    print("[INFO] Mapping network {} predicts: {}".format(MAPPING_CONCEPTS[i], res))

    concepts_res.append((MAPPING_CONCEPTS[i], res))

filename = './temp/observations/observation.txt'
with open(filename, 'w') as observations_file:
    observations_file.write(_to_original_observation(CONCEPTS_MAP[main_res], PREFIX))

    for res in concepts_res:
        observations_file.write(_to_mapping_observation(CONCEPTS_MAP[res[0]], PREFIX, res[1]))

    observations_file.close()

print("[INFO] Finished")
