from skimage.io import imread
from tensorflow.keras.models import load_model

import argparse
import numpy as np
import pandas as pd

"""
Note: for reproducibility set the environment variable PYTHONHASHSEED to value 0, and CUDA_VISIBLE_DEVICES to value "".
"""

CONCEPTS_MAP = {
    'TypeA': 'having a type A train',
    'TypeB': 'having a type B train',
    'TypeC': 'having a type C train',

    'LongWagon': 'having a long wagon',
    'PassengerCar': 'having a passenger car',
    'FreightWagon': 'having a freight car',
    'EmptyWagon': 'having an empty wagon',

    'LongTrain': 'having a long train',
    'WarTrain': 'having a war train',
    'PassengerTrain': 'having a passenger train',
    'LongPassengerTrain': 'having a long passenger train',

    'FreightTrain': 'having a freight train',
    'LongFreightTrain': 'having a long freight train',
    'EmptyTrain': 'having an empty train',
    'RuralTrain': 'having a cosy train',

    'MixedTrain': 'having a mixed train',
    'OpenRoof': 'having an open roof car',
    'ReinforcedCar': 'having a reinforced car',
    'ThreeWheelsWagon': 'having a wagon with three wheels',
}

# program arguments and constants, to be changed depending on the experiment
parser = argparse.ArgumentParser()
parser.add_argument('-nn', '--nn', help='path to neural network model (.hdf5)', type=str, required=True)
parser.add_argument('-i', '--input', help='path to neural network\'s input (.png)', type=str, required=True)

args = parser.parse_args()

PATH_TO_ORIGINAL_NN = args.nn
TARGET_IMAGE = args.input

OUT_FILE = './temp/observations/classification.txt'
PATH_TO_CSV = './datasets/explainable_abstract_trains_subset/trains_subset.csv'

if 'Type_A' in PATH_TO_ORIGINAL_NN:
    ORIGINAL_CONCEPT = 'TypeA'
    LAYERS_TO_EXTRACT = ['batch_normalization_5', 'batch_normalization_6', 'batch_normalization_7',
                         'batch_normalization_8']
elif 'Type_B' in PATH_TO_ORIGINAL_NN:
    ORIGINAL_CONCEPT = 'TypeB'
    LAYERS_TO_EXTRACT = ['batch_normalization_5', 'batch_normalization_6', 'batch_normalization_7']
elif 'Type_C' in PATH_TO_ORIGINAL_NN:
    ORIGINAL_CONCEPT = 'TypeC'
    LAYERS_TO_EXTRACT = ['batch_normalization_5', 'batch_normalization_6', 'batch_normalization_7',
                         'batch_normalization_8', 'batch_normalization_9']


def main():
    image_num = int(TARGET_IMAGE.split('/')[-1].split('\\')[-1][:-4])

    print("[INFO] Loading input image...")
    original_img = imread(TARGET_IMAGE)

    original_img = np.asarray([original_img], dtype=np.float32)
    original_img /= 255.0

    print("[INFO] Loading original network...")
    model = load_model(PATH_TO_ORIGINAL_NN)
    original_res = model.predict(original_img)
    original_res = original_res[0][0]
    original_res = round(original_res)

    label = pd.read_csv(PATH_TO_CSV, usecols=['name', ORIGINAL_CONCEPT], na_values=['NA', '?'])
    label = label.loc[label['name'] == image_num, [ORIGINAL_CONCEPT]]
    label = label.values[0][0]

    if original_res == label:
        guessed = 'correctly'
    else:
        guessed = 'incorrectly'

    if original_res == 0:
        classification = 'not ' + CONCEPTS_MAP[ORIGINAL_CONCEPT]
    else:
        classification = CONCEPTS_MAP[ORIGINAL_CONCEPT]

    with open(OUT_FILE, 'w') as classification_file:
        classification_file.write('The original network {} classified the input as an image {}.'.format(
            guessed, classification))

        classification_file.close()

    print("[INFO] Finished")


if __name__ == '__main__':
    main()
