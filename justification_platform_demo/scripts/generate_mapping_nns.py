from operator import itemgetter

import argparse
import gc
import multiprocessing
import numpy as np
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from utils.data_handling.trains_data_handler import extract_activations_dataset
from utils.neurosym_utils import evaluate_model, tf_set_memory_growth, train_model, train_val_test_split
from utils.nn import Linear, Simple
from utils.seed_generator import SeedGenerator

"""
This program checks the accuracy of the small networks built for each concept of interest, for a given neural network,
 if needed it starts by extracting the activations from the neural network.
This script is used to understand which concepts are more easily extractable from the given neural network,
 from a given set of layers.
This script performs multiple runs of the same experiment.
Note: for reproducibility set the environment variable PYTHONHASHSEED to value 0, and CUDA_VISIBLE_DEVICES to value "".
"""

# program arguments and constants, to be changed depending on the experiment
parser = argparse.ArgumentParser()
parser.add_argument('-nn', '--nn', help='path to neural network model (.hdf5)', type=str, required=True)
parser.add_argument('-c', '--concepts', nargs='+', help='concepts to map', type=str, required=True)

args = parser.parse_args()

PATH_TO_NN = args.nn
OUTPUT_CONCEPTS = args.concepts

if 'Type_A' in PATH_TO_NN:
    LAYERS_TO_EXTRACT = ['batch_normalization_5', 'batch_normalization_6', 'batch_normalization_7',
                         'batch_normalization_8']
elif 'Type_B' in PATH_TO_NN:
    LAYERS_TO_EXTRACT = ['batch_normalization_5', 'batch_normalization_6', 'batch_normalization_7']
elif 'Type_C' in PATH_TO_NN:
    LAYERS_TO_EXTRACT = ['batch_normalization_5', 'batch_normalization_6', 'batch_normalization_7',
                         'batch_normalization_8', 'batch_normalization_9']

ARCHITECTURES = [Simple, Linear]  # architectures being tested

TRAIN_SET_SIZE = 800
VALIDATION_SET_SIZE = 200
TEST_SET_SIZE = 1_000

PATIENCE = 15
MAX_EPOCHS = 400
BATCH_SIZE = 32

PATH_TO_IMAGES = './datasets/explainable_abstract_trains_subset/npz_images/'
PATH_TO_CSV = './datasets/explainable_abstract_trains_subset/trains_subset.csv'
PATH_TO_WEIGHTS = './temp/mapping_nns/'


def get_activations(queue, output_concept, path_to_nn, layers_to_extract, dataset_size,
                    samples_by_value, path_to_imgs, path_to_csv, np_seed, tf_seed):
    # initialize random seeds
    np.random.seed(np_seed)

    import tensorflow as tf
    from tensorflow.keras.models import load_model

    # enable tensorflow gpu memory growth
    tf_set_memory_growth()
    tf.random.set_seed(tf_seed)

    # load the model to analyze
    original_model = load_model(path_to_nn)

    # extract activations from neural network and don't store them to disk
    print("[INFO] Extracting activations from neural network...")
    df = extract_activations_dataset(original_model, layers_to_extract, dataset_size, output_concept,
                                     samples_by_value, path_to_csv, path_to_imgs)

    queue.put(df)


def check_architecture(queue, output_concept, architecture, col_names, x_train, y_train, x_val, y_val, x_test, y_test,
                       patience, max_epochs, batch_size, path_to_weights, seed):
    # enable tensorflow gpu memory growth
    tf_set_memory_growth()
    import tensorflow as tf
    tf.random.set_seed(seed)

    # build the model
    print("[INFO] Compiling model...")
    print("[INFO] # of inputs = {}".format(len(col_names)))
    model = architecture.build(num_inputs=len(col_names), classes=1)
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

    # train the model
    print("[INFO] Training model...")
    weights_name = path_to_weights + '{}_{}.hdf5'.format(output_concept, seed)
    (model, H) = train_model(model, x_train, y_train, x_val, y_val, max_epochs, patience, batch_size, weights_name)

    # evaluate the model
    print("[INFO] Evaluating network...")
    (tr_score, val_score, test_score) = evaluate_model(model, x_train, y_train, x_val, y_val, x_test, y_test)
    print("[INFO] Train accuracy: {}".format(tr_score))
    print("[INFO] Validation accuracy: {}".format(val_score))
    print("[INFO] Test accuracy: {}".format(test_score))

    file_name = '{}_{}_{}_t{}_v{}_s{}'.format(output_concept, test_score, architecture.__name__,
                                              len(y_train), len(y_val), seed)

    # remove temporary files
    os.remove(weights_name)

    # save best trained model, put accuracy in filename
    weights_name = path_to_weights + file_name + '.hdf5'
    model.save(weights_name)

    # clean memory
    del H
    del model
    gc.collect()

    # keep the score of the architecture
    queue.put((architecture.__name__, val_score, test_score, weights_name))


def check_concept(output_concept, architectures, path_to_nn, layers_to_extract, patience, tr_size, val_size, test_size,
                  seed_generator, path_to_imgs, path_to_csv, path_to_weights, max_epochs, batch_size):
    dataset_size = tr_size + val_size + test_size
    samples_by_value = {0: dataset_size // 2, 1: dataset_size - dataset_size // 2}

    # if needed create folders to store the results
    os.makedirs(path_to_weights, exist_ok=True)

    # initialize random seeds
    np.random.seed(seed_generator.next())

    # obtain the activations from the original neural network
    df_queue = multiprocessing.Queue()
    args = (df_queue, output_concept, path_to_nn, layers_to_extract, dataset_size, samples_by_value,
            path_to_imgs, path_to_csv, seed_generator.next(), seed_generator.next())
    p = multiprocessing.Process(target=get_activations, args=args)
    p.start()
    df = df_queue.get()
    p.join()
    p.terminate()
    df_queue.close()

    col_names = [col for col in df.columns if any([layer in col for layer in layers_to_extract])]
    data = df.loc[:, col_names].values.astype(np.float32)

    print("[INFO] Loading labels from dataset...")
    labels = df[[output_concept]].values.astype(np.bool_)
    labels = np.array(labels, dtype=np.bool_)

    # clean memory
    del df, p, df_queue
    gc.collect()

    # partition the data into training, validation and testing splits
    print("[INFO] splitting data for training and testing...")
    (x_train, x_val, x_test, y_train, y_val, y_test) = train_val_test_split(
        data, labels, tr_size, val_size, test_size, random_seed=seed_generator.next(), shuffle=True)

    print("[INFO] # training examples: {}".format(len(y_train)))
    print("[INFO] # validation examples: {}".format(len(y_val)))
    print("[INFO] # testing examples: {}".format(len(y_test)))

    # clean memory
    del data, labels
    gc.collect()

    # keep the score of each architecture
    scores = []

    score_queue = multiprocessing.Queue()
    processes = []

    for architecture in architectures:
        args = (score_queue, output_concept, architecture, col_names, x_train, y_train, x_val, y_val, x_test, y_test,
                patience, max_epochs, batch_size, path_to_weights, seed_generator.next())
        p = multiprocessing.Process(target=check_architecture, args=args)
        processes.append(p)
        p.start()

    for p in processes:
        scores.append(score_queue.get())
        p.join()
        p.terminate()

    score_queue.close()

    # clean memory
    del processes, score_queue
    gc.collect()

    # sort the trained models by their performance in the validation set
    scores = sorted(scores, key=itemgetter(1), reverse=True)

    print("Architecture \t\t Validation Accuracy \t Test Accuracy")
    for score in scores:
        print("{:<20.19s} {:<20} {:<20}".format(score[0], score[1], score[2]))

    # delete the remaining networks
    for score in scores[1:]:
        os.remove(score[3])

    # rename the best performing network
    weights_name = path_to_weights + '{}.hdf5'.format(output_concept)
    os.replace(scores[0][3], weights_name)


def main():
    seed_generator = SeedGenerator()

    for concept in OUTPUT_CONCEPTS:
        check_concept(concept, ARCHITECTURES, PATH_TO_NN, LAYERS_TO_EXTRACT, PATIENCE,
                      TRAIN_SET_SIZE, VALIDATION_SET_SIZE, TEST_SET_SIZE, seed_generator, PATH_TO_IMAGES, PATH_TO_CSV,
                      PATH_TO_WEIGHTS, MAX_EPOCHS, BATCH_SIZE)

    print("[INFO] Finished")


if __name__ == '__main__':
    main()
