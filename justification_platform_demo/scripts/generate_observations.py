from skimage.io import imread
from tensorflow.keras.models import load_model

import argparse
import numpy as np
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from utils.activation_extractor import ActivationExtractor

"""

"""

PREFIX = '__input__ Type: '
NEGATION = 'not '

CONCEPTS_MAP = {
    'TypeA': '(TypeA)',
    'TypeB': '(TypeB)',
    'TypeC': '(TypeC)',

    'LongWagon': '(has some LongWagon)',
    'PassengerCar': '(has some PassengerCar)',
    'FreightWagon': '(has some FreightWagon)',
    'EmptyWagon': '(has some EmptyWagon)',

    'LongTrain': '(LongTrain)',
    'WarTrain': '(WarTrain)',
    'PassengerTrain': '(PassengerTrain)',
    'LongPassengerTrain': '(LongPassengerTrain)',

    'FreightTrain': '(FreightTrain)',
    'LongFreightTrain': '(LongFreightTrain)',
    'EmptyTrain': '(EmptyTrain)',
    'CosyTrain': '(CosyTrain)',

    'MixedTrain': '(MixedTrain)',
    'OpenRoof': '(has some OpenRoofCar)',
    'ReinforcedCar': '(has some ReinforcedCar)',
    'ThreeWheelsWagon': '(has some ThreeWheelsWagon)',
}

# program arguments and constants, to be changed depending on the experiment
parser = argparse.ArgumentParser()
parser.add_argument('-nn', '--nn', help='path to neural network model (.hdf5)', type=str, required=True)
parser.add_argument('-i', '--input', help='path to neural network\'s input (.png)', type=str, required=True)
parser.add_argument('-c', '--concepts', nargs='+', help='concepts to map', type=str, required=True)

args = parser.parse_args()

PATH_TO_ORIGINAL_NN = args.nn
TARGET_IMAGE = args.input
MAPPING_CONCEPTS = args.concepts

if 'Type_A' in PATH_TO_ORIGINAL_NN:
    ORIGINAL_CONCEPT = 'TypeA'
    LAYERS_TO_EXTRACT = ['batch_normalization_5', 'batch_normalization_6', 'batch_normalization_7',
                         'batch_normalization_8']
elif 'Type_B' in PATH_TO_ORIGINAL_NN:
    ORIGINAL_CONCEPT = 'TypeB'
    LAYERS_TO_EXTRACT = ['batch_normalization_5', 'batch_normalization_6', 'batch_normalization_7']
elif 'Type_C' in PATH_TO_ORIGINAL_NN:
    ORIGINAL_CONCEPT = 'TypeC'
    LAYERS_TO_EXTRACT = ['batch_normalization_5', 'batch_normalization_6', 'batch_normalization_7',
                         'batch_normalization_8', 'batch_normalization_9']

PATH_TO_WEIGHTS = './mapping_nns/'


def _to_original_observation(concept, prefix, confidence):
    if confidence >= 0.5:
        return prefix + concept + '\n'
    else:
        return prefix + _not(concept) + '\n'


def _to_mapping_observation(concept, prefix, confidence):
    if confidence >= 0.5:
        return prefix + concept + ", " + str(confidence) + '\n'
    else:
        return prefix + _not(concept) + ", " + str((1-confidence)) + '\n'


def _not(text):
    return NEGATION + text


print("[INFO] Loading input image...")
original_img = imread(TARGET_IMAGE)

original_img = np.asarray([original_img], dtype=np.float32)
original_img /= 255.0

print("[INFO] Loading original network...")
model = load_model(PATH_TO_ORIGINAL_NN)
print("[INFO] Loading mapping networks...")
mapping_nns = []
for concept in MAPPING_CONCEPTS:
    path_to_nn = PATH_TO_WEIGHTS + concept + '.hdf5'
    mapping_nns.append(load_model(path_to_nn))

# predict the original image output
original_res = model.predict(original_img)
original_res = original_res[0][0]
print("[INFO] Main model predicts: {}".format(original_res))

# extract activations from the occluded dataset for the chosen layers
extractor = ActivationExtractor(model, LAYERS_TO_EXTRACT)
activations_original = extractor.extract_activations(original_img)

del model

concepts_res = []
for i, nn in enumerate(mapping_nns):
    # predict the original image output
    res = nn.predict(activations_original)
    res = res[0][0]
    print("[INFO] Mapping network {} predicts: {}".format(MAPPING_CONCEPTS[i], res))

    concepts_res.append((MAPPING_CONCEPTS[i], res))

filename = './temp/observations/observation.txt'
with open(filename, 'w') as observations_file:
    observations_file.write(_to_original_observation(CONCEPTS_MAP[ORIGINAL_CONCEPT], PREFIX, original_res))

    for res in concepts_res:
        observations_file.write(_to_mapping_observation(CONCEPTS_MAP[res[0]], PREFIX, res[1]))

    observations_file.close()

print("[INFO] Finished")
