from operator import itemgetter

import argparse
import gc
import multiprocessing
import numpy as np
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from utils.data_handling.gtsrb_data_handler import extract_activations_dataset
from utils.neurosym_utils import evaluate_model, tf_set_memory_growth, train_model, train_val_test_split
from utils.nn import Linear, Simple
from utils.seed_generator import SeedGenerator

from sklearn.model_selection import train_test_split


"""
This program checks the accuracy of the small networks built for each concept of interest, for a given neural network,
 if needed it starts by extracting the activations from the neural network.
This script is used to understand which concepts are more easily extractable from the given neural network,
 from a given set of layers.
This script performs multiple runs of the same experiment.
Note: for reproducibility set the environment variable PYTHONHASHSEED to value 0, and CUDA_VISIBLE_DEVICES to value "".
"""

# program arguments and constants, to be changed depending on the experiment
parser = argparse.ArgumentParser()
parser.add_argument('-nn', '--nn', help='path to neural network model (.hdf5)', type=str, required=True)
parser.add_argument('-c', '--concepts', nargs='+', help='concepts to map', type=str, required=True)

args = parser.parse_args()

PATH_TO_NN = args.nn
OUTPUT_CONCEPTS = args.concepts

LAYERS_TO_EXTRACT = [
    'block_1_project_BN',
    'block_2_project_BN',
    'block_3_project_BN',
    'block_4_project_BN',
    'block_5_project_BN',
    'block_6_project_BN',
    'block_7_project_BN',
    'block_8_project_BN',
    'block_9_project_BN',
    'block_10_project_BN',
    'block_11_project_BN',
    'block_12_project_BN',
    'block_13_project_BN',
    'block_14_project_BN',
    'block_15_project_BN',
    'block_16_project_BN'
]

ARCHITECTURES = [Simple, Linear]  # architectures being tested

TRAIN_SET_SIZE = 280
VALIDATION_SET_SIZE = 70
TEST_SET_SIZE = 100

PATIENCE = 15
MAX_EPOCHS = 400
BATCH_SIZE = 32

PATH_TO_IMAGES_TRAIN = './datasets/gtsrb_subset/train/'
PATH_TO_IMAGES_TEST = './datasets/gtsrb_subset/test/'
PATH_TO_CSV_TRAIN = './datasets/gtsrb_subset/GT-final_train.csv'
PATH_TO_CSV_TEST = './datasets/gtsrb_subset/GT-final_test.csv'
PATH_TO_WEIGHTS = './temp/mapping_nns/'


def _adjust_samples_by_value(samples_by_value, size, mode, output_concept):
    if size == 350 and mode == 'train' and output_concept == 'Symbol_NoEntryGoods':
        missing = 9
        samples_by_value = {0: size // 2 + missing, 1: size - size // 2 - missing}
    elif size == 350 and mode == 'train' and output_concept == 'Symbol_Speed100':
        missing = 11
        samples_by_value = {0: size // 2 + missing, 1: size - size // 2 - missing}
    elif size == 350 and mode == 'train' and output_concept == 'Symbol_Speed120':
        missing = 29
        samples_by_value = {0: size // 2 + missing, 1: size - size // 2 - missing}
    elif size == 100 and mode == 'test' and output_concept == 'Symbol_Speed120':
        missing = 7
        samples_by_value = {0: size // 2 + missing, 1: size - size // 2 - missing}

    return samples_by_value


def get_activations(queue, output_concept, path_to_nn, layers_to_extract, dataset_size,
                    samples_by_value, path_to_imgs, path_to_csv, np_seed, tf_seed):
    # initialize random seeds
    np.random.seed(np_seed)

    import tensorflow as tf
    from tensorflow.keras.models import load_model

    # enable tensorflow gpu memory growth
    tf_set_memory_growth()
    tf.random.set_seed(tf_seed)

    # load main network
    main_model = load_model(path_to_nn)
    main_model = main_model.layers[1]

    # extract activations from main network (note: here activations are not stored to disk)
    print("[INFO] Extracting main networks' activations...")
    print(output_concept);
    print(dataset_size);
    print(samples_by_value);
    df = extract_activations_dataset(main_model, layers_to_extract, path_to_csv, path_to_imgs, num_samples=dataset_size,
                                     balance_class=output_concept, samples_by_value=samples_by_value,
                                     cols=['Filename', output_concept])

    queue.put(df)


def check_architecture(queue, output_concept, architecture, col_names, x_train, y_train, x_val, y_val, x_test, y_test,
                       patience, max_epochs, batch_size, path_to_weights, seed):
    # enable tensorflow gpu memory growth
    tf_set_memory_growth()
    import tensorflow as tf
    tf.random.set_seed(seed)

    # build the model
    print("[INFO] Compiling model...")
    print("[INFO] # of inputs = {}".format(len(col_names)))
    model = architecture.build(num_inputs=len(col_names), classes=1)
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

    # train the model
    print("[INFO] Training model...")
    weights_name = path_to_weights + '{}_{}.hdf5'.format(output_concept, seed)
    (model, H) = train_model(model, x_train, y_train, x_val, y_val, max_epochs, patience, batch_size, weights_name)

    # evaluate the model
    print("[INFO] Evaluating network...")
    (tr_score, val_score, test_score) = evaluate_model(model, x_train, y_train, x_val, y_val, x_test, y_test)
    print("[INFO] Train accuracy: {}".format(tr_score))
    print("[INFO] Validation accuracy: {}".format(val_score))
    print("[INFO] Test accuracy: {}".format(test_score))

    file_name = '{}_{}_{}_t{}_v{}_s{}'.format(output_concept, test_score, architecture.__name__,
                                              len(y_train), len(y_val), seed)

    # remove temporary files
    os.remove(weights_name)

    # save best trained model, put accuracy in filename
    weights_name = path_to_weights + file_name + '.hdf5'
    model.save(weights_name)

    # clean memory
    del H
    del model
    gc.collect()

    # keep the score of the architecture
    queue.put((architecture.__name__, val_score, test_score, weights_name))


def check_concept(output_concept, architectures, path_to_nn, layers_to_extract, patience, tr_size, val_size, test_size,
                  seed_generator, path_to_imgs_train, path_to_imgs_test, path_to_csv_train, path_to_csv_test, path_to_weights, max_epochs, batch_size):
    remain_size = tr_size + val_size
    samples_by_value = {0: remain_size // 2, 1: remain_size - remain_size // 2}
    samples_by_value = _adjust_samples_by_value(samples_by_value, remain_size, 'train', output_concept)

    # if needed create folders to store the results
    os.makedirs(path_to_weights, exist_ok=True)

    # initialize random seeds
    np.random.seed(seed_generator.next())

    # obtain the activations from the original neural network
    df_queue = multiprocessing.Queue()
    args = (df_queue, output_concept, path_to_nn, layers_to_extract, remain_size, samples_by_value,
            path_to_imgs_train, path_to_csv_train, seed_generator.next(), seed_generator.next())
    p = multiprocessing.Process(target=get_activations, args=args)
    p.start()
    df_remain = df_queue.get()
    p.join()
    p.terminate()

    col_names = [col for col in df_remain.columns if any([layer in col for layer in layers_to_extract])]
    x_remain = df_remain.loc[:, col_names].values.astype(np.float32)

    samples_by_value = {0: test_size // 2, 1: test_size - test_size // 2}
    samples_by_value = _adjust_samples_by_value(samples_by_value, test_size, 'test', output_concept)
    args = (df_queue, output_concept, path_to_nn, layers_to_extract, test_size, samples_by_value, path_to_imgs_test,
            path_to_csv_test, seed_generator.next(), seed_generator.next())
    p = multiprocessing.Process(target=get_activations, args=args)
    p.start()
    df_test = df_queue.get()
    p.join()
    p.terminate()

    x_test = df_test.loc[:, col_names].values.astype(np.float32)

    df_queue.close()

    print("[INFO] Loading labels from dataset...")
    y_remain = df_remain[[output_concept]].values.astype(np.bool_)
    y_test = df_test[[output_concept]].values.astype(np.bool_)

    # clean memory
    del df_remain, df_test, p, df_queue
    gc.collect()

    # partition the data into training, validation and testing splits
    print("[INFO] Splitting data for training, validation, and testing...")
    (x_train, x_val, y_train, y_val) = train_test_split(
        x_remain, y_remain, train_size=tr_size, test_size=val_size,
        random_state=seed_generator.next(), shuffle=True, stratify=y_remain)

    print("[INFO] # training examples: {}".format(len(y_train)))
    print("[INFO] # validation examples: {}".format(len(y_val)))
    print("[INFO] # testing examples: {}".format(len(y_test)))

    # clean memory
    del x_remain, y_remain
    gc.collect()

    # keep the score of each architecture
    scores = []

    score_queue = multiprocessing.Queue()
    processes = []

    for architecture in architectures:
        args = (score_queue, output_concept, architecture, col_names, x_train, y_train, x_val, y_val, x_test, y_test,
                patience, max_epochs, batch_size, path_to_weights, seed_generator.next())
        p = multiprocessing.Process(target=check_architecture, args=args)
        processes.append(p)
        p.start()

    for p in processes:
        scores.append(score_queue.get())
        p.join()
        p.terminate()

    score_queue.close()

    # clean memory
    del processes, score_queue
    gc.collect()

    # sort the trained models by their performance in the validation set
    scores = sorted(scores, key=itemgetter(1), reverse=True)

    print("Architecture \t\t Validation Accuracy \t Test Accuracy")
    for score in scores:
        print("{:<20.19s} {:<20} {:<20}".format(score[0], score[1], score[2]))

    # delete the remaining networks
    for score in scores[1:]:
        os.remove(score[3])

    # rename the best performing network
    weights_name = path_to_weights + '{}.hdf5'.format(output_concept)
    os.replace(scores[0][3], weights_name)


def main():
    seed_generator = SeedGenerator()

    for concept in OUTPUT_CONCEPTS:
        check_concept(concept, ARCHITECTURES, PATH_TO_NN, LAYERS_TO_EXTRACT, PATIENCE,
                      TRAIN_SET_SIZE, VALIDATION_SET_SIZE, TEST_SET_SIZE, seed_generator, PATH_TO_IMAGES_TRAIN,
                      PATH_TO_IMAGES_TEST, PATH_TO_CSV_TRAIN, PATH_TO_CSV_TEST, PATH_TO_WEIGHTS, MAX_EPOCHS, BATCH_SIZE)

    print("[INFO] Finished")


if __name__ == '__main__':
    main()
